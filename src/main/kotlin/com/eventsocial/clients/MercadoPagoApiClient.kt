package com.eventsocial.clients

import com.eventsocial.clients.domain.response.CollectionMPClient
import com.eventsocial.domain.entities.Booking
import com.eventsocial.domain.requests.CreatePaymentMP
import com.mercadopago.MercadoPago
import com.mercadopago.resources.Preference
import com.mercadopago.resources.Preference.AutoReturn
import com.mercadopago.resources.datastructures.preference.BackUrls
import com.mercadopago.resources.datastructures.preference.Item
import com.mercadopago.resources.datastructures.preference.Payer
import org.springframework.http.*
import org.springframework.stereotype.Repository
import org.springframework.web.client.RestTemplate
import java.util.*

@Repository
class MercadoPagoApiClient {

    var restTemplate = RestTemplate()

    private val ACCESS_TOKEN_KEY = "APP_USR-7477459527147951-082201-870637e4280c5179745e129d40393b95-344059923"

    fun paymentsMP(createPaymentMP: CreatePaymentMP, userCode: UUID, booking: Booking): Preference {
        val preference = Preference()

        val url = BackUrls()
        url.success = createPaymentMP.urlBack // TODO: verificar donde re-dirigir
        MercadoPago.SDK.setAccessToken(ACCESS_TOKEN_KEY)

        val item = Item()
        item.setTitle(booking.spaceId.title).setQuantity(1).unitPrice = booking.amount.toFloat()
        preference.appendItem(item)
        preference.autoReturn = AutoReturn.all
        preference.backUrls = url
        preference.externalReference = (userCode.toString() + booking.spaceId.code + booking.dateCheckIn)
        preference.notificationUrl = "https://eventuy.herokuapp.com/event-social-back/v1/payments/statusChange/MP"

        val payer = Payer()
        payer.email = booking.userId.registerId.email

        preference.payer = Payer()
        preference.save()

        return preference
    }

    fun dataPaymentMP(url: String): ResponseEntity<CollectionMPClient> {
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON
        headers.contentType = MediaType.APPLICATION_JSON
        headers.accept = listOf(MediaType.APPLICATION_JSON)
        headers["Authorization"] = "Bearer $ACCESS_TOKEN_KEY"
        val requestEntity: HttpEntity<*> = HttpEntity<Any?>(headers)
        return restTemplate.exchange(url, HttpMethod.GET, requestEntity, CollectionMPClient::class.java)
    }
}
