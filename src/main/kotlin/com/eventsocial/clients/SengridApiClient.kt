package com.eventsocial.clients

import com.eventsocial.domain.entities.Booking
import com.eventsocial.domain.entities.Order
import com.eventsocial.domain.entities.Spaces
import com.eventsocial.domain.entities.Users
import com.eventsocial.domain.requests.CreateBooking
import com.eventsocial.domain.requests.CreateOrder
import com.sendgrid.Method
import com.sendgrid.Request
import com.sendgrid.SendGrid
import com.sendgrid.helpers.mail.Mail
import com.sendgrid.helpers.mail.objects.Email
import com.sendgrid.helpers.mail.objects.Personalization
import org.springframework.stereotype.Repository
import java.io.IOException
import java.time.Instant
import java.time.ZoneId
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter

@Repository
class SengridApiClient {

    val SENDGRID_API_KEY = "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"
    val TEMPLATE_ID_CONSULT = "d-f0aa934a3b4c41508e535e913d56cb0b"
    val TEMPLATE_ID_WELCOME = "d-48ea7aba25784e3dbe87cf79914e0b85"
    val TEMPLATE_ID_BOOKING_IN_PROGRESS_ORGANIZER = "d-999d4d4464014859813a87320ae577ec"
    val TEMPLATE_ID_BOOKING_IN_PROGRESS_OWNER = "d-5330406e73fc47e9a31307b6d950a4f8"
    val TEMPLATE_OTP = "d-9820134e2d39456aabcc62ded0c88d91"

    fun postSendEmail(email: String?, userName: String?) {
        val from = Email("eventuy.app@gmail.com")
        val to = Email(email!!.trim { it <= ' ' })
        val personalization = Personalization()
        personalization.addBcc(from)
        personalization.addTo(to)
        val map = HashMap<String, String?>()
        map["nombre"] = userName
        personalization.addDynamicTemplateData("data", map)
        val mail = Mail()
        mail.setFrom(from)
        mail.setReplyTo(to)
        mail.setTemplateId(TEMPLATE_ID_WELCOME)
        mail.addPersonalization(personalization)
        val sg = SendGrid(SENDGRID_API_KEY)
        val request = Request()
        try {
            request.method = Method.POST
            request.endpoint = "mail/send"
            request.body = mail.build()
            val response = sg.api(request)
            println(response.statusCode)
            println(response.body)
            println(response.headers)
        } catch (ex: IOException) {
            throw ex
        }
    }

    fun postOTPEmail(email: String, otp: String) {
        val from = Email("eventuy.app@gmail.com")
        val to = Email(email)
        val personalization = Personalization()
        personalization.addBcc(from)
        personalization.addTo(to)
        val map = HashMap<String, String>()
        map["otp_code"] = otp
        personalization.addDynamicTemplateData("data", map)
        val mail = Mail()
        mail.setFrom(from)
        mail.setReplyTo(to)
        mail.setTemplateId(TEMPLATE_OTP)
        mail.addPersonalization(personalization)
        val sg = SendGrid(SENDGRID_API_KEY)
        val request = Request()
        try {
            request.method = Method.POST
            request.endpoint = "mail/send"
            request.body = mail.build()
            val response = sg.api(request)
            println(response.statusCode)
            println(response.body)
            println(response.headers)
        } catch (ex: IOException) {
            throw ex
        }
    }

    fun sendOwnerBooking(
        space: Spaces,
        createBooking: CreateBooking,
        organizer: Users,
        ownerInfo: Users
    ) {
        val from = Email("eventuy.app@gmail.com")
        val to = Email(ownerInfo.registerId.email)
        val personalization = Personalization()
        personalization.addBcc(from)
        personalization.addTo(to)
        val map = HashMap<String, String?>()

        map["nombre_inmueble"] = space.title
        map["url_redirect"] = createBooking.urlRedirect
        map["fecha_reserva"] = createBooking.date.dateCheckIn.toString()
        map["huespedes"] = createBooking.occupancy.toString()
        map["hora_reserva"] = createBooking.date.dateCheckIn.atZone(ZoneOffset.UTC).hour.toString()
        map["precio_total"] = createBooking.amount.toString()
        map["nombre_usuario"] = organizer.name

        personalization.addDynamicTemplateData("data", map)

        val mail = Mail()
        mail.setFrom(from)
        mail.setReplyTo(to)
        mail.setTemplateId(TEMPLATE_ID_BOOKING_IN_PROGRESS_OWNER)
        mail.addPersonalization(personalization)

        val sg = SendGrid(SENDGRID_API_KEY)
        val request = Request()

        request.method = Method.POST
        request.endpoint = "mail/send"
        request.body = mail.build()
        val response = sg.api(request)
        println(response.statusCode)
        println(response.body)
        println(response.headers)
    }

    fun bookingStatusOrganizer(booking: Booking, urlRedirect: String?) {

        val from = Email("eventuy.app@gmail.com")
        val to = Email(booking.userId.registerId.email)

        val personalization = Personalization()

        personalization.addBcc(from)
        personalization.addTo(to)

        val map = HashMap<String, String?>()

        val statusConfirmacion = when (booking.ownerApproval) {
            "CREATED" -> "A confirmar por propietario"
            "ACCEPTED" -> "Reserva confirmada por el propietario"
            else -> "Reserva rechazada por el propietario"
        }

        map["nombre_inmueble"] = booking.spaceId.title
        map["url_redirect"] = urlRedirect
        map["fecha_reserva"] =
            DateTimeFormatter.ofPattern("MM-dd-yyyy").withZone(ZoneId.of("UTC")).format(booking.dateCheckIn)
                .toString()
        map["huespedes"] = booking.occupancy.toString()
        map["hora_reserva"] =
            DateTimeFormatter.ofPattern("hh:mm:ss").withZone(ZoneId.of("UTC")).format(booking.dateCheckIn)
                .toString()
        map["estado_confirmacion"] = statusConfirmacion
        map["direccion_inmueble"] = booking.spaceId.street + booking.spaceId.number + ", " + booking.spaceId.province
        map["precio_reserva"] = booking.amount.toString()

        map["precio_total"] = booking.spaceId.price.toString()
        map["precio_total_a_pagar"] = booking.spaceId.price.toString()
        personalization.addDynamicTemplateData("data", map)
        val mail = Mail()
        mail.setFrom(from)
        mail.setReplyTo(to)
        mail.setTemplateId(TEMPLATE_ID_BOOKING_IN_PROGRESS_ORGANIZER)
        mail.addPersonalization(personalization)

        val sg = SendGrid(SENDGRID_API_KEY)
        val request = Request()
        try {
            request.method = Method.POST
            request.endpoint = "mail/send"
            request.body = mail.build()
            val response = sg.api(request)
            println(response.statusCode)
            println(response.body)
            println(response.headers)
        } catch (ex: IOException) {
            throw ex
        }
    }

    fun bookingStatusOwner(
        booking: Booking
    ) {
        val from = Email("eventuy.app@gmail.com")
        val to = Email(booking.ownerUserId.registerId.email)
        val personalization = Personalization()
        personalization.addBcc(from)
        personalization.addTo(to)
        val map = HashMap<String, String?>()
        map["nombre_inmueble"] = booking.spaceId.title
        map["fecha_reserva"] =
            DateTimeFormatter.ofPattern("MM-dd-yyyy").withZone(ZoneId.of("UTC")).format(booking.dateCheckIn)
                .toString()
        map["huespedes"] = booking.occupancy.toString()
        map["hora_reserva"] =
            DateTimeFormatter.ofPattern("hh:mm:ss").withZone(ZoneId.of("UTC")).format(booking.dateCheckIn)
                .toString()
        map["precio_total"] = booking.amount.toString()
        map["nombre_usuario"] = booking.ownerUserId.name
        personalization.addDynamicTemplateData("data", map)
        val mail = Mail()
        mail.setFrom(from)
        mail.setReplyTo(to)
        mail.setTemplateId(TEMPLATE_ID_BOOKING_IN_PROGRESS_OWNER)
        mail.addPersonalization(personalization)
        val sg = SendGrid(SENDGRID_API_KEY)
        val request = Request()
        try {
            request.method = Method.POST
            request.endpoint = "mail/send"
            request.body = mail.build()
            val response = sg.api(request)
            println(response.statusCode)
            println(response.body)
            println(response.headers)
        } catch (ex: IOException) {
            throw ex
        }
    }

    fun sendOwnerOrder(
        createOrder: CreateOrder,
        organizer: Users,
        ownerInfo: Users
    ) {
        val from = Email("eventuy.app@gmail.com")
        val to = Email(ownerInfo.registerId.email)
        val personalization = Personalization()
        personalization.addBcc(from)
        personalization.addTo(to)
        val map = HashMap<String, String?>()

        map["nombre_inmueble"] = ""
        map["url_redirect"] = createOrder.urlRedirect
        map["fecha_reserva"] = Instant.now().atZone(ZoneOffset.UTC).hour.toString()
        map["huespedes"] = ""
        map["hora_reserva"] = ""
        map["precio_total"] = createOrder.totalPrice.toString()
        map["nombre_usuario"] = organizer.name

        personalization.addDynamicTemplateData("data", map)

        val mail = Mail()
        mail.setFrom(from)
        mail.setReplyTo(to)
        mail.setTemplateId(TEMPLATE_ID_BOOKING_IN_PROGRESS_OWNER)
        mail.addPersonalization(personalization)

        val sg = SendGrid(SENDGRID_API_KEY)
        val request = Request()

        request.method = Method.POST
        request.endpoint = "mail/send"
        request.body = mail.build()
        val response = sg.api(request)
        println(response.statusCode)
        println(response.body)
        println(response.headers)
    }

    fun orderStatusOrganizer(order: Order, urlRedirect: String?) {
        val from = Email("eventuy.app@gmail.com")
        val to = Email(order.user.registerId.email)

        val personalization = Personalization()

        personalization.addBcc(from)
        personalization.addTo(to)

        val map = HashMap<String, String?>()

        val statusConfirmacion = when (order.ownerApproval) {
            "CREATED" -> "A confirmar por propietario"
            "ACCEPTED" -> "Reserva confirmada por el propietario"
            else -> "Reserva rechazada por el propietario"
        }
        map["precio_total"] = order.totalPrice.toString()
        personalization.addDynamicTemplateData("data", map)
        val mail = Mail()
        mail.setFrom(from)
        mail.setReplyTo(to)
        mail.setTemplateId(TEMPLATE_ID_BOOKING_IN_PROGRESS_ORGANIZER)
        mail.addPersonalization(personalization)

        val sg = SendGrid(SENDGRID_API_KEY)
        val request = Request()
        try {
            request.method = Method.POST
            request.endpoint = "mail/send"
            request.body = mail.build()
            val response = sg.api(request)
            println(response.statusCode)
            println(response.body)
            println(response.headers)
        } catch (ex: IOException) {
            throw ex
        }
    }

    fun orderStatusOwner(
        booking: Booking
    ) {
        val from = Email("eventuy.app@gmail.com")
        val to = Email(booking.ownerUserId.registerId.email)
        val personalization = Personalization()
        personalization.addBcc(from)
        personalization.addTo(to)
        val map = HashMap<String, String?>()
        map["nombre_inmueble"] = booking.spaceId.title
        map["fecha_reserva"] =
            DateTimeFormatter.ofPattern("MM-dd-yyyy").withZone(ZoneId.of("UTC")).format(booking.dateCheckIn)
                .toString()
        map["huespedes"] = booking.occupancy.toString()
        map["hora_reserva"] =
            DateTimeFormatter.ofPattern("hh:mm:ss").withZone(ZoneId.of("UTC")).format(booking.dateCheckIn)
                .toString()
        map["precio_total"] = booking.amount.toString()
        map["nombre_usuario"] = booking.ownerUserId.name
        personalization.addDynamicTemplateData("data", map)
        val mail = Mail()
        mail.setFrom(from)
        mail.setReplyTo(to)
        mail.setTemplateId(TEMPLATE_ID_BOOKING_IN_PROGRESS_OWNER)
        mail.addPersonalization(personalization)
        val sg = SendGrid(SENDGRID_API_KEY)
        val request = Request()
        try {
            request.method = Method.POST
            request.endpoint = "mail/send"
            request.body = mail.build()
            val response = sg.api(request)
            println(response.statusCode)
            println(response.body)
            println(response.headers)
        } catch (ex: IOException) {
            throw ex
        }
    }
}
