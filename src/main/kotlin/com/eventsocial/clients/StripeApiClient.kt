package com.eventsocial.clients

import com.eventsocial.domain.entities.Booking
import com.eventsocial.domain.entities.Order
import com.eventsocial.domain.requests.CreateBookingPaymentStripe
import com.eventsocial.domain.requests.CreateOrderPaymentStripe
import com.stripe.Stripe
import com.stripe.model.*
import com.stripe.model.checkout.Session
import com.stripe.param.PriceCreateParams
import com.stripe.param.ProductCreateParams
import com.stripe.param.checkout.SessionCreateParams
import com.stripe.param.checkout.SessionCreateParams.PaymentIntentData
import org.springframework.stereotype.Repository
import spark.Spark.port
import java.util.*

@Repository
class StripeApiClient {

    fun createPayment(createBookingPaymentStripe: CreateBookingPaymentStripe, userCode: UUID, booking: Booking): Session? {

        port(4242)
        Stripe.apiKey =
            "sk_test_51MMKPDHySarhzIo6wfq5G5ddHivoGj85VbjZLNRj10Ky8MQGx8IVWr6n0i72AMhdyMC1QkrwK4cwVGAcnqxmk5Wc00zOc2E8KC"

        val productCreateParams = ProductCreateParams.builder()
            .setName(booking.spaceId.title)
            .setDescription(booking.dateCheckIn.toString())
            .build()
        val product: Product = Product.create(productCreateParams)

        val priceCreateParams = PriceCreateParams.builder()
            .setProduct(product.id)
            .setUnitAmount((booking.amount * 100).toLong())
            .setCurrency(booking.currency)
            .build()
        val price = Price.create(priceCreateParams)

        val paymentIntentData = PaymentIntentData.builder()
            .putMetadata("booking_code", booking.code.toString())
            .build()

        val params: SessionCreateParams = SessionCreateParams.builder()
            .addPaymentMethodType(SessionCreateParams.PaymentMethodType.CARD)
            .addLineItem(
                SessionCreateParams.LineItem.builder()
                    .setPrice(price.id)
                    .setQuantity(1L)
                    .build()
            )
            .setPaymentIntentData(paymentIntentData)
            .setMode(SessionCreateParams.Mode.PAYMENT)
            .setSuccessUrl(createBookingPaymentStripe.urlBack)
            .setCancelUrl(createBookingPaymentStripe.urlBack)
            .setClientReferenceId(booking.code.toString()) // Agrega los metadatos personalizados
            .build()

        return Session.create(params)
    }

    fun createOrderPayment(createOrderPaymentStripe: CreateOrderPaymentStripe, userCode: UUID, order: Order): Session? {
        port(4242)
        Stripe.apiKey =
            "sk_test_51MMKPDHySarhzIo6wfq5G5ddHivoGj85VbjZLNRj10Ky8MQGx8IVWr6n0i72AMhdyMC1QkrwK4cwVGAcnqxmk5Wc00zOc2E8KC"
        val productCreateParams = ProductCreateParams.builder()
            .setName("Pedido numero :" + order.id)
            .setDescription(order.id.toString())
            .build()
        val product: Product = Product.create(productCreateParams)

        val priceCreateParams = PriceCreateParams.builder()
            .setProduct(product.id)
            .setUnitAmount((order.totalPrice * 100).toLong())
            .setCurrency(order.currency)
            .build()
        val price = Price.create(priceCreateParams)

        val paymentIntentData = PaymentIntentData.builder()
            .putMetadata("order_code", order.code.toString())
            .build()

        val params: SessionCreateParams = SessionCreateParams.builder()
            .addPaymentMethodType(SessionCreateParams.PaymentMethodType.CARD)
            .addLineItem(
                SessionCreateParams.LineItem.builder()
                    .setPrice(price.id)
                    .setQuantity(1L)
                    .build()
            )
            .setPaymentIntentData(paymentIntentData)
            .setMode(SessionCreateParams.Mode.PAYMENT)
            .setSuccessUrl(createOrderPaymentStripe.urlBack)
            .setCancelUrl(createOrderPaymentStripe.urlBack)
            .setClientReferenceId(order.code.toString())
            .build()
        return Session.create(params)
    }
}
