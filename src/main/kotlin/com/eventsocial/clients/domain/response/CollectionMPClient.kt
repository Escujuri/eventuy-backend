package com.eventsocial.clients.domain.response

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

data class CollectionMPClient(
    @JsonProperty("collection")
    val collection: CollectionMP?
)

data class CollectionMP(
    @JsonProperty("id")
    val paymentId: Long?,

    @JsonProperty("date_created")
    val date_created: Date?,

    @JsonProperty("date_approved")
    val date_approved: Date?,

    @JsonProperty("payer")
    val payer: PayerMP?,

    @JsonProperty("status")
    val status: String?,

    @JsonProperty("status_detail")
    val status_detail: String?,

    @JsonProperty("transaction_amount")
    val transaction_amount: Float?,

    @JsonProperty("net_received_amount")
    val net_received_amount: Float?,

    @JsonProperty("currency_id")
    val currency_id: String?,

    @JsonProperty("payment_type")
    val paymentType: String?,

    @JsonProperty("payment_method_id")
    val paymentMethodId: String?,

    @JsonProperty("collector")
    val collector: CollectorMP?,

    @JsonProperty("external_reference")
    val external_reference: String?,
)

data class PayerMP(

    @JsonProperty("id")
    val id: String?,

    @JsonProperty("email")
    val email: String?,

    @JsonProperty("identification")
    val identification: IdentificationMP?,

    @JsonProperty("type")
    val type: String?,
)

data class IdentificationMP(
    @JsonProperty("type")
    val type: String?,

    @JsonProperty("number")
    val number: String?,
)

data class CollectorMP(
    @JsonProperty("id")
    val collectorId: Long? = null
)
