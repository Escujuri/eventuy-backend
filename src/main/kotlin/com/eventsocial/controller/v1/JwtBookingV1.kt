package com.eventsocial.controller.v1

import com.eventsocial.domain.requests.ChangeStatusBookingRequest
import com.eventsocial.domain.requests.CreateBooking
import com.eventsocial.domain.response.BookingResponse
import com.eventsocial.model.GenericUtils
import com.eventsocial.security.jwt.ITokenExtractor
import com.eventsocial.security.jwt.JwtTokenUtil
import com.eventsocial.services.BookingService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.data.domain.Page
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.ws.rs.QueryParam

@PreAuthorize("authenticated")
@CrossOrigin
@RestController
@RequestMapping(value = ["v1/booking"], produces = [MediaType.APPLICATION_JSON_VALUE])
@Validated
@Api(value = "", tags = ["Recusos de administracion de reservas"], protocols = "https")
class JwtBookingV1 {

    @Autowired
    private lateinit var bookingService: BookingService

    @Autowired
    @Qualifier("jwtHeaderTokenExtractor")
    private lateinit var tokenExtractor: ITokenExtractor

    @PreAuthorize("hasRole('USER')")
    @ApiOperation(value = "Genera una reserva")
    @PostMapping
    @ResponseBody
    fun createBooking(
        request: HttpServletRequest,
        @RequestBody(required = true)
        @Validated createBooking: CreateBooking
    ): ResponseEntity<Unit> {

        var userCode: UUID? = null

        val claims = tokenExtractor.ReadToken(tokenExtractor.Extract(request.getHeader(JwtTokenUtil.TOKEN_HEADER)))

        if (!claims.isNullOrEmpty()) {
            if (GenericUtils.getValueFromKeyValue(claims, "userCode") != null) {
                userCode = UUID.fromString(GenericUtils.getValueFromKeyValue(claims, "userCode").toString())
            }
        }
        return ResponseEntity(bookingService.createBooking(createBooking, userCode!!), HttpStatus.CREATED)
    }

    @PreAuthorize("hasRole('USER')")
    @ApiOperation(value = "Recurso que obtiene las reservas, paginado")
    @GetMapping
    @ResponseBody
    fun getBookingByUserCode(
        request: HttpServletRequest,
        @RequestParam("page") page: Int?,
        @RequestParam("offset") offset: Int?,
        @QueryParam("owner") owner: Boolean?,
        @QueryParam("status") status: BookingService.StatusOwnerApproval?,
    ): ResponseEntity<Page<BookingResponse>> {

        var userCode: UUID? = null

        val claims = tokenExtractor.ReadToken(tokenExtractor.Extract(request.getHeader(JwtTokenUtil.TOKEN_HEADER)))

        if (!claims.isNullOrEmpty()) {
            if (GenericUtils.getValueFromKeyValue(claims, "userCode") != null) {
                userCode = UUID.fromString(GenericUtils.getValueFromKeyValue(claims, "userCode").toString())
            }
        }
        return ResponseEntity(bookingService.getBookingByUserCode(userCode!!, owner ?: false, status, offset ?: 10, page ?: 0), HttpStatus.OK)
    }

    @PreAuthorize("hasRole('USER')")
    @ApiOperation(value = "Recuso que actualiza el estado (REJECTED, ACCEPTED) de una reserva. Solo uso para el dueño del space")
    @PostMapping("/{bookingCode}/status/{status}")
    @ResponseBody
    fun pathBookingByBookingCode(
        request: HttpServletRequest,
        @PathVariable("bookingCode", required = true) bookingCode: UUID,
        @PathVariable("status", required = true) statusOwnerApproval: BookingService.StatusOwnerApproval,
        @RequestBody(required = true)
        @Validated changeStatusBookingRequest: ChangeStatusBookingRequest
    ): ResponseEntity<BookingResponse> {
        var userCode: UUID? = null

        val claims = tokenExtractor.ReadToken(tokenExtractor.Extract(request.getHeader(JwtTokenUtil.TOKEN_HEADER)))

        if (!claims.isNullOrEmpty()) {
            if (GenericUtils.getValueFromKeyValue(claims, "userCode") != null) {
                userCode = UUID.fromString(GenericUtils.getValueFromKeyValue(claims, "userCode").toString())
            }
        }
        return ResponseEntity(bookingService.pathBookingByBookingCode(userCode!!, bookingCode, statusOwnerApproval, changeStatusBookingRequest.urlRedirect), HttpStatus.OK)
    }

    @PreAuthorize("hasRole('USER')")
    @ApiOperation(value = "Recurso que obtiene las reservas por usuario, paginado")
    @GetMapping("/{bookingCode}")
    @ResponseBody
    fun getBookingByCode(
        request: HttpServletRequest,
        @PathVariable("bookingCode", required = true) bookingCode: UUID
    ): ResponseEntity<BookingResponse> {
        var userId: Int = 0
        val claims = tokenExtractor.ReadToken(tokenExtractor.Extract(request.getHeader(JwtTokenUtil.TOKEN_HEADER)))

        if (!claims.isNullOrEmpty()) {
            if (GenericUtils.getValueFromKeyValue(claims, "userId") != null) {
                userId = Integer.valueOf(GenericUtils.getValueFromKeyValue(claims, "userId").toString())
            }
        }
        return ResponseEntity(bookingService.getBookingByCode(userId, bookingCode), HttpStatus.OK)
    }
}
