package com.eventsocial.controller.v1

import com.eventsocial.domain.entities.Company
import com.eventsocial.domain.mappers.CompanyMapper.toCompanyReponse
import com.eventsocial.domain.requests.CompanySummaryDTO
import com.eventsocial.domain.requests.CreateCompanyDTO
import com.eventsocial.domain.requests.UpdateCompanyDTO
import com.eventsocial.domain.response.CompanyReponse
import com.eventsocial.domain.response.CompanySumaryReponse
import com.eventsocial.model.GenericUtils
import com.eventsocial.security.jwt.ITokenExtractor
import com.eventsocial.security.jwt.JwtTokenUtil
import com.eventsocial.services.CompanyService
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.data.domain.Page
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.servlet.http.HttpServletRequest

@PreAuthorize("authenticated")
@CrossOrigin
@RestController
@Validated
@RequestMapping("v1/companies")
class JwtCompanyController {

    @Autowired
    lateinit var companyService: CompanyService

    @Autowired
    @Qualifier("jwtHeaderTokenExtractor")
    private lateinit var tokenExtractor: ITokenExtractor

    @PreAuthorize("hasRole('USER')")
    @ApiOperation(value = "Genera una compania con servicios")
    @PostMapping
    @ResponseBody
    fun createCompany(
        request: HttpServletRequest,
        @RequestBody company: CreateCompanyDTO
    ): ResponseEntity<Company> {
        var userCode: UUID? = null

        val claims = tokenExtractor.ReadToken(tokenExtractor.Extract(request.getHeader(JwtTokenUtil.TOKEN_HEADER)))

        if (!claims.isNullOrEmpty()) {
            if (GenericUtils.getValueFromKeyValue(claims, "userCode") != null) {
                userCode = UUID.fromString(GenericUtils.getValueFromKeyValue(claims, "userCode").toString())
            }
        }
        val createdCompany = companyService.createCompany(userCode!!, company)
        return ResponseEntity(createdCompany, HttpStatus.CREATED)
    }

    @PreAuthorize("hasRole('USER')")
    @ApiOperation(value = "Obtiene las companias por usuario")
    @GetMapping("/user")
    @ResponseBody
    fun getCompaniesByUserCode(
        request: HttpServletRequest,
        @RequestParam("page") page: Int?,
        @RequestParam("offset") offset: Int?
    ): ResponseEntity<Page<CompanySumaryReponse?>> {
        var userCode: UUID? = null
        val claims = tokenExtractor.ReadToken(tokenExtractor.Extract(request.getHeader(JwtTokenUtil.TOKEN_HEADER)))
        if (!claims.isNullOrEmpty()) {
            if (GenericUtils.getValueFromKeyValue(claims, "userCode") != null) {
                userCode = UUID.fromString(GenericUtils.getValueFromKeyValue(claims, "userCode").toString())
            }
        }
        return ResponseEntity(companyService.getCompanyByUserCode(userCode!!, offset ?: 10, page ?: 0), HttpStatus.OK)
    }

    @GetMapping("/{companyCode}")
    fun getCompanyByCode(
        @PathVariable("companyCode", required = true) companyCode: UUID,
    ): ResponseEntity<CompanyReponse> {
        val company = companyService.getCompanyByCode(companyCode)?.toCompanyReponse()
        return if (company != null)
            ResponseEntity(company, HttpStatus.OK)
        else
            ResponseEntity(HttpStatus.NOT_FOUND)
    }

    @PreAuthorize("hasRole('USER')")
    @PutMapping("/{companyCode}")
    fun updateCompany(
        request: HttpServletRequest,
        @PathVariable("companyCode", required = true) companyCode: UUID,
        @RequestBody updatedCompany: UpdateCompanyDTO
    ): ResponseEntity<CompanyReponse> {
        var userCode: UUID? = null

        val claims = tokenExtractor.ReadToken(tokenExtractor.Extract(request.getHeader(JwtTokenUtil.TOKEN_HEADER)))

        if (!claims.isNullOrEmpty()) {
            if (GenericUtils.getValueFromKeyValue(claims, "userCode") != null) {
                userCode = UUID.fromString(GenericUtils.getValueFromKeyValue(claims, "userCode").toString())
            }
        }
        val company = companyService.updateCompany(companyCode, updatedCompany, userCode)
        return if (company != null) {
            ResponseEntity(company, HttpStatus.OK)
        } else {
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }
    @PreAuthorize("hasRole('USER')")
    @DeleteMapping("/{companyCode}")
    fun deleteCompany(
        request: HttpServletRequest,
        @PathVariable("companyCode", required = true) companyCode: UUID,
    ): ResponseEntity<Unit> {

        var userCode: UUID? = null

        val claims = tokenExtractor.ReadToken(tokenExtractor.Extract(request.getHeader(JwtTokenUtil.TOKEN_HEADER)))

        if (!claims.isNullOrEmpty()) {
            if (GenericUtils.getValueFromKeyValue(claims, "userCode") != null) {
                userCode = UUID.fromString(GenericUtils.getValueFromKeyValue(claims, "userCode").toString())
            }
        }

        companyService.deleteCompany(companyCode, userCode)
        return ResponseEntity(HttpStatus.OK)
    }

    @GetMapping
    fun getAllCompanies(
        @RequestParam(required = false) serviceCategory: String?,
        @RequestParam(required = false) withShipping: Boolean?,
        @RequestParam(required = false) minPrice: Double?,
        @RequestParam(required = false) maxPrice: Double?,
        @RequestParam(required = false) searchQuery: String?,
        @RequestParam("page") page: Int?,
        @RequestParam("offset") offset: Int?
    ): Page<CompanySummaryDTO> {
        return companyService.getAllCompanies(serviceCategory, withShipping, minPrice, maxPrice, searchQuery, offset ?: 10, page ?: 0)
    }
}
