package com.eventsocial.controller.v1

import com.eventsocial.domain.mappers.CompanyMapper.toCompanyReponse
import com.eventsocial.domain.mappers.SpaceMapper.toFilterSpacesReponse
import com.eventsocial.domain.requests.SpacesTypes
import com.eventsocial.security.jwt.ITokenExtractor
import com.eventsocial.services.FilterService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import java.math.BigDecimal
import java.util.*
import javax.servlet.http.HttpServletRequest

@RestController
@Validated
@CrossOrigin
@Api(value = "", tags = ["Home filters"], protocols = "https")
@RequestMapping(value = ["v1/filter"])
class JwtHomeControllerV1 {

    @Autowired
    private lateinit var filterService: FilterService

    @Autowired
    @Qualifier("jwtHeaderTokenExtractor")
    lateinit var tokenExtractor: ITokenExtractor

    @PreAuthorize("hasRole('USER')")
    @ApiOperation(value = "Recurso para obtenes los spacios por filtros")
    @GetMapping("/spaces")
    @ResponseBody
    fun getFilterSpaces(
        request: HttpServletRequest,
        @RequestParam("spaceType") spaceType: List<SpacesTypes>?,
        @RequestParam("maximum_occupancy") maximumOccupancy: Int?,
        @RequestParam("priceMin") priceMin: BigDecimal?,
        @RequestParam("priceMax") priceMax: BigDecimal?,
        @RequestParam(name = "country", required = false) country: List<String>?,
        @RequestParam(name = "province", required = false) provinces: List<String>?,
        @RequestParam(name = "ranking", required = false) ranking: Int?,
        @RequestParam(name = "services", required = false) services: List<String>?,
        @RequestParam("page") page: Int?,
        @RequestParam("offset") offset: Int?,
        @RequestParam(name = "sortBy", required = false, defaultValue = "ranking") sortBy: String,
        @RequestParam(name = "sortOrder", required = false, defaultValue = "asc") sortOrder: String,
    ) = filterService.getFilterSpaces(spaceType, maximumOccupancy, priceMin, priceMax, country, provinces, ranking, services,page ?: 0, offset ?: 10, sortBy, sortOrder).map { it.toFilterSpacesReponse() }

    @PreAuthorize("hasRole('USER')")
    @ApiOperation(value = "Recurso para obtenes los spacios por filtros")
    @GetMapping("/companies")
    @ResponseBody
    fun getFilterCompanies(
        request: HttpServletRequest,
        @RequestParam(name = "category", required = false) categories: List<String>?,
        @RequestParam(name = "province", required = false) provinces: List<String>?,
        @RequestParam(name = "freeShipping", required = false) freeShipping: Boolean?,
        @RequestParam("page") page: Int?,
        @RequestParam("offset") offset: Int?,
        @RequestParam(name = "sortBy", required = false, defaultValue = "ranking") sortBy: String?,
        @RequestParam(name = "sortOrder", required = false, defaultValue = "asc") sortOrder: String?,
    ) = filterService.getCompaniesByMultipleFilters(categories, provinces, freeShipping, offset ?: 10, page ?: 0, sortBy, sortOrder).map { it.toCompanyReponse() }
}
