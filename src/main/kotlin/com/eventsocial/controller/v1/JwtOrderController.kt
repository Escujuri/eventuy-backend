package com.eventsocial.controller.v1

import com.eventsocial.domain.requests.ChangeStatusBookingRequest
import com.eventsocial.domain.requests.CreateOrder
import com.eventsocial.domain.response.OrderResponse
import com.eventsocial.model.GenericUtils
import com.eventsocial.security.jwt.ITokenExtractor
import com.eventsocial.security.jwt.JwtTokenUtil
import com.eventsocial.services.BookingService
import com.eventsocial.services.OrderService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.ws.rs.QueryParam

@PreAuthorize("authenticated")
@CrossOrigin
@RestController
@RequestMapping(value = ["v1/order"], produces = [MediaType.APPLICATION_JSON_VALUE])
@Validated
@Api(value = "", tags = ["Manejo de creacion de ordenes"], protocols = "https")
class JwtOrderController {

    @Autowired
    @Qualifier("jwtHeaderTokenExtractor")
    private lateinit var tokenExtractor: ITokenExtractor

    @Autowired
    private lateinit var orderService: OrderService

    @PreAuthorize("hasRole('USER')")
    @ApiOperation(value = "Recurso que obtiene las ordener, paginado")
    @GetMapping
    fun getOrderByUserCode(
        request: HttpServletRequest,
        @RequestParam("page") page: Int?,
        @RequestParam("offset") offset: Int?,
        @QueryParam("owner") owner: Boolean?,
        @QueryParam("status") status: BookingService.StatusOwnerApproval?,
    ): Any {
        var userCode: UUID? = null

        val tokenPayload = tokenExtractor.Extract(request.getHeader(JwtTokenUtil.TOKEN_HEADER))
        val claims = tokenExtractor.ReadToken(tokenPayload)

        if (!claims.isNullOrEmpty()) {
            if (GenericUtils.getValueFromKeyValue(claims, "userCode") != null) {
                userCode = UUID.fromString(GenericUtils.getValueFromKeyValue(claims, "userCode").toString())
            }
        }
        return ResponseEntity(
            orderService.getOrderByUserCode(userCode!!, owner ?: false, status, offset ?: 10, page ?: 0), HttpStatus.OK
        )
    }

    @PreAuthorize("hasRole('USER')")
    @ApiOperation(value = "Recurso que obtiene las ordene por code")
    @GetMapping("/{orderCode}")
    fun getOrderByCode(
        request: HttpServletRequest,
        @PathVariable("orderCode", required = true) orderCode: UUID
    ): Any {
        return ResponseEntity(
            orderService.getOrderByCode(orderCode), HttpStatus.OK
        )
    }

    @PreAuthorize("hasRole('USER')")
    @ApiOperation(value = "")
    @PostMapping
    fun createOrder(
        request: HttpServletRequest,
        @RequestBody(required = true) createOrder: CreateOrder
    ): ResponseEntity<Unit> {
        var userCode: UUID? = null

        val tokenPayload = tokenExtractor.Extract(request.getHeader(JwtTokenUtil.TOKEN_HEADER))
        val claims = tokenExtractor.ReadToken(tokenPayload)

        if (!claims.isNullOrEmpty()) {
            if (GenericUtils.getValueFromKeyValue(claims, "userCode") != null) {
                userCode = UUID.fromString(GenericUtils.getValueFromKeyValue(claims, "userCode").toString())
            }
        }
        return ResponseEntity(
            orderService.createOrder(userCode!!, createOrder), HttpStatus.CREATED
        )
    }

    @PreAuthorize("hasRole('USER')")
    @ApiOperation(value = "Recuso que actualiza el estado (REJECTED, ACCEPTED) de una orden. Solo uso para el dueño de la orden")
    @PostMapping("/{orderCode}/status/{status}")
    @ResponseBody
    fun pathBookingByBookingCode(
        request: HttpServletRequest,
        @PathVariable("orderCode", required = true) orderCode: UUID,
        @PathVariable("status", required = true) statusOwnerApproval: OrderService.StatusOwnerApproval,
        @RequestBody(required = true)
        @Validated changeStatusBookingRequest: ChangeStatusBookingRequest
    ): ResponseEntity<OrderResponse> {
        var userCode: UUID? = null

        val claims = tokenExtractor.ReadToken(tokenExtractor.Extract(request.getHeader(JwtTokenUtil.TOKEN_HEADER)))

        if (!claims.isNullOrEmpty()) {
            if (GenericUtils.getValueFromKeyValue(claims, "userCode") != null) {
                userCode = UUID.fromString(GenericUtils.getValueFromKeyValue(claims, "userCode").toString())
            }
        }
        return ResponseEntity(orderService.pathOrderByOrderCode(userCode!!, orderCode, statusOwnerApproval, changeStatusBookingRequest.urlRedirect), HttpStatus.OK)
    }
}
