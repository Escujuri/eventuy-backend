package com.eventsocial.controller.v1

import com.eventsocial.domain.entities.Transactional
import com.eventsocial.domain.requests.CreateBookingPaymentStripe
import com.eventsocial.domain.requests.CreateOrderPaymentStripe
import com.eventsocial.domain.requests.CreatePaymentMP
import com.eventsocial.domain.requests.StatusChangePaymentMP
import com.eventsocial.domain.response.PaymentMP
import com.eventsocial.domain.response.PaymentStripe
import com.eventsocial.model.GenericUtils
import com.eventsocial.security.jwt.ITokenExtractor
import com.eventsocial.security.jwt.JwtTokenUtil
import com.eventsocial.services.PaymentsService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.servlet.http.HttpServletRequest

@PreAuthorize("authenticated")
@CrossOrigin
@RestController
@RequestMapping(value = ["v1/payments"], produces = [MediaType.APPLICATION_JSON_VALUE])
@Validated
@Api(value = "", tags = ["Pasarela de pago MERCADO PAGO"], protocols = "https")
class JwtPaymentsV1 {

    @Autowired
    private lateinit var paymentService: PaymentsService

    @Autowired
    @Qualifier("jwtHeaderTokenExtractor")
    private lateinit var tokenExtractor: ITokenExtractor

    @PreAuthorize("hasRole('USER')")
    @ApiOperation(value = "Se realiza el pago, via MERCADO PAGO. Devolviendo la url de re-direccionamiento")
    @PostMapping("/MP")
    @ResponseBody
    fun paymentMP(
        request: HttpServletRequest,
        @RequestBody(required = true)
        @Validated createPaymentMP: CreatePaymentMP
    ): ResponseEntity<PaymentMP> {
        var userCode: UUID? = null

        val tokenPayload = tokenExtractor.Extract(request.getHeader(JwtTokenUtil.TOKEN_HEADER))
        val claims = tokenExtractor.ReadToken(tokenPayload)

        if (!claims.isNullOrEmpty()) {
            if (GenericUtils.getValueFromKeyValue(claims, "userCode") != null) {
                userCode = UUID.fromString(GenericUtils.getValueFromKeyValue(claims, "userCode").toString())
            }
        }
        return ResponseEntity(paymentService.paymentsMPService(createPaymentMP, userCode!!), HttpStatus.OK)
    }

    @PostMapping("/statusChange/MP")
    @ApiOperation(value = "De uso interno")
    @ResponseBody
    fun statusChangePayment(
        request: HttpServletRequest,
        @RequestBody(required = true)
        @Validated statusChangeInfo: StatusChangePaymentMP
    ) = paymentService.statuChangePayment(statusChangeInfo)

    @PreAuthorize("hasRole('USER')")
    @ApiOperation(value = "Recuso que trae la informacion de la transaccion de una reserva realizada")
    @GetMapping("/booking/{bookingCode}/organizer")
    @ResponseBody
    fun getPaymentMPByBookingCodeOrganizer(
        request: HttpServletRequest,
        @RequestBody(required = true)
        @PathVariable("bookingCode", required = true) bookingCode: UUID
    ): ResponseEntity<Transactional> {
        var userCode: UUID? = null

        val tokenPayload = tokenExtractor.Extract(request.getHeader(JwtTokenUtil.TOKEN_HEADER))
        val claims = tokenExtractor.ReadToken(tokenPayload)

        if (!claims.isNullOrEmpty()) {
            if (GenericUtils.getValueFromKeyValue(claims, "userCode") != null) {
                userCode = UUID.fromString(GenericUtils.getValueFromKeyValue(claims, "userCode").toString())
            }
        }
        return ResponseEntity(paymentService.getPaymentMPByBookingCodeOrganizer(bookingCode, userCode!!), HttpStatus.OK)
    }

    @PreAuthorize("hasRole('USER')")
    @ApiOperation(value = "Recuso que trae la informacion de la transaccion de una reserva realizada")
    @GetMapping("/booking/{bookingCode}/owner")
    @ResponseBody
    fun getPaymentMPByBookingCodeOwner(
        request: HttpServletRequest,
        @RequestBody(required = true)
        @PathVariable("bookingCode", required = true) bookingCode: UUID
    ): ResponseEntity<Transactional> {
        var userCode: UUID? = null

        val tokenPayload = tokenExtractor.Extract(request.getHeader(JwtTokenUtil.TOKEN_HEADER))
        val claims = tokenExtractor.ReadToken(tokenPayload)

        if (!claims.isNullOrEmpty()) {
            if (GenericUtils.getValueFromKeyValue(claims, "userCode") != null) {
                userCode = UUID.fromString(GenericUtils.getValueFromKeyValue(claims, "userCode").toString())
            }
        }
        return ResponseEntity(paymentService.getPaymentMPByBookingCodeOwner(bookingCode, userCode!!), HttpStatus.OK)
    }

    @PreAuthorize("hasRole('USER')")
    @ApiOperation(value = "Se realiza el pago del booking, via Stripe. retornando la url de redireccionamiento")
    @PostMapping("/stripe")
    @ResponseBody
    fun createPaymentStripeByBookingId(
        request: HttpServletRequest,
        @RequestBody(required = true)
        @Validated createBookingPaymentStripe: CreateBookingPaymentStripe
    ): ResponseEntity<PaymentStripe> {
        var userCode: UUID? = null

        val tokenPayload = tokenExtractor.Extract(request.getHeader(JwtTokenUtil.TOKEN_HEADER))
        val claims = tokenExtractor.ReadToken(tokenPayload)

        if (!claims.isNullOrEmpty()) {
            if (GenericUtils.getValueFromKeyValue(claims, "userCode") != null) {
                userCode = UUID.fromString(GenericUtils.getValueFromKeyValue(claims, "userCode").toString())
            }
        }
        return ResponseEntity(paymentService.createPaymentStripeByBookingId(createBookingPaymentStripe, userCode!!), HttpStatus.OK)
    }

    @PreAuthorize("hasRole('USER')")
    @ApiOperation(value = "Se realiza el pago de la orden, via Stripe. retornando la url de redireccionamiento")
    @PostMapping("/stripe/order")
    @ResponseBody
    fun createPaymentStripeByOrder(
        request: HttpServletRequest,
        @RequestBody(required = true)
        @Validated createOrderPaymentStripe: CreateOrderPaymentStripe
    ): ResponseEntity<PaymentStripe> {
        var userCode: UUID? = null

        val tokenPayload = tokenExtractor.Extract(request.getHeader(JwtTokenUtil.TOKEN_HEADER))
        val claims = tokenExtractor.ReadToken(tokenPayload)

        if (!claims.isNullOrEmpty()) {
            if (GenericUtils.getValueFromKeyValue(claims, "userCode") != null) {
                userCode = UUID.fromString(GenericUtils.getValueFromKeyValue(claims, "userCode").toString())
            }
        }
        return ResponseEntity(paymentService.createPaymentStripeByOrder(createOrderPaymentStripe, userCode!!), HttpStatus.OK)
    }

    @PostMapping("/stripe/webhook")
    @ResponseBody
    fun stripeWebhookEvent(
        request: HttpServletRequest,
        @RequestBody(required = true) eventJson: String,
        @RequestHeader("Stripe-Signature") signature: String
    ): ResponseEntity<String> {
        paymentService.stripeWebhookEvent(eventJson, signature)
        return ResponseEntity.ok("Evento procesado exitosamente.")
    }
}
