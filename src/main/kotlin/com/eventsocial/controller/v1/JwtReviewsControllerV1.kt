package com.eventsocial.controller.v1

import com.eventsocial.domain.mappers.ReviewMapper.toReviewCompanyReponse
import com.eventsocial.domain.mappers.ReviewMapper.toReviewSpaceReponse
import com.eventsocial.domain.requests.CreateReviewsCompanyRequest
import com.eventsocial.domain.requests.CreateReviewsRequest
import com.eventsocial.domain.response.ReviewCompanyReponse
import com.eventsocial.domain.response.ReviewSpaceReponse
import com.eventsocial.model.GenericUtils
import com.eventsocial.security.jwt.ITokenExtractor
import com.eventsocial.security.jwt.JwtTokenUtil
import com.eventsocial.services.ReviewsService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.servlet.http.HttpServletRequest

@RestController
@Validated
@CrossOrigin
@Api(value = "Reviews de spaces", tags = ["Reviews de spaces"], protocols = "https")
@RequestMapping(value = ["v1/reviews"])
class JwtReviewsControllerV1 {

    @Autowired
    private lateinit var reviewsService: ReviewsService

    @Autowired
    @Qualifier("jwtHeaderTokenExtractor")
    lateinit var tokenExtractor: ITokenExtractor

    @CrossOrigin
    @PostMapping
    @ApiOperation(value = "Crea una review de un space por usuario")
    fun createReviewSpace(
        request: HttpServletRequest,
        @RequestBody(required = true) createReviewsRequest: CreateReviewsRequest
    ): ResponseEntity<ReviewSpaceReponse> {
        var userCode: UUID? = null

        val tokenPayload = tokenExtractor.Extract(request.getHeader(JwtTokenUtil.TOKEN_HEADER))
        val claims = tokenExtractor.ReadToken(tokenPayload)

        if (!claims.isNullOrEmpty()) {
            if (GenericUtils.getValueFromKeyValue(claims, "userCode") != null) {
                userCode = UUID.fromString(GenericUtils.getValueFromKeyValue(claims, "userCode").toString())
            }
        }
        return ResponseEntity(reviewsService.createReview(userCode!!, createReviewsRequest), HttpStatus.OK)
    }

    @CrossOrigin
    @GetMapping("/{spaceCode}")
    @ApiOperation(value = "Devuelve una review de un space por usuario")
    fun getReviewByOrgCode(
        request: HttpServletRequest,
        @PathVariable("spaceCode", required = true) spaceCode: UUID
    ) = reviewsService.getReviewsBySpaceCode(spaceCode).map {
        it?.toReviewSpaceReponse()
    }

    @CrossOrigin
    @PostMapping("/company")
    @ApiOperation(value = "Crea una review de un space por usuario")
    fun createReview(
        request: HttpServletRequest,
        @RequestBody(required = true) createReviewsCompanyRequest: CreateReviewsCompanyRequest
    ): ResponseEntity<ReviewCompanyReponse> {
        var userCode: UUID? = null

        val tokenPayload = tokenExtractor.Extract(request.getHeader(JwtTokenUtil.TOKEN_HEADER))
        val claims = tokenExtractor.ReadToken(tokenPayload)

        if (!claims.isNullOrEmpty()) {
            if (GenericUtils.getValueFromKeyValue(claims, "userCode") != null) {
                userCode = UUID.fromString(GenericUtils.getValueFromKeyValue(claims, "userCode").toString())
            }
        }
        return ResponseEntity(reviewsService.createReviewCompany(userCode!!, createReviewsCompanyRequest), HttpStatus.OK)
    }

    @CrossOrigin
    @GetMapping("/{companyCode}/company")
    @ApiOperation(value = "Devuelve una review de una compañia por code")
    fun getReviewByCompanyCode(
        request: HttpServletRequest,
        @PathVariable("companyCode", required = true) companyCode: UUID
    ) = reviewsService.getReviewsByCompanyCode(companyCode).map {
        it?.toReviewCompanyReponse()
    }
}
