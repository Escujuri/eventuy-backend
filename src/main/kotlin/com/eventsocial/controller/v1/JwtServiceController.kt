package com.eventsocial.controller.v1

import com.eventsocial.domain.entities.Services
import com.eventsocial.services.ServicesService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import java.net.URI
import java.util.*

@CrossOrigin
@RestController
@RequestMapping(value = ["v1/service"], produces = [MediaType.APPLICATION_JSON_VALUE])
@Validated
@Api(value = "", tags = ["Manejo de creacion de servicios"], protocols = "https")
class JwtServiceController {

    @Autowired
    private lateinit var serviceService: ServicesService

    @GetMapping
    fun getAllServices(): List<Services> {
        return serviceService.getAllServices()
    }

    @PostMapping
    fun createService(@RequestBody service: Services): ResponseEntity<Services> {
        val createdService = serviceService.createService(service)
        return ResponseEntity.created(URI("/service/${createdService.id}")).body(createdService)
    }

    @PutMapping("/{id}")
    fun updateService(@PathVariable id: Long, @RequestBody service: Services): ResponseEntity<Services> {
        val updatedService = serviceService.updateService(id, service)
        return if (updatedService != null) {
            ResponseEntity.ok(updatedService)
        } else {
            ResponseEntity.notFound().build()
        }
    }

    @GetMapping("/{id}")
    fun getServiceById(@PathVariable id: Long): ResponseEntity<Services> {
        val service = serviceService.getServiceById(id)
        return if (service != null) {
            ResponseEntity.ok(service)
        } else {
            ResponseEntity.notFound().build()
        }
    }

    @DeleteMapping("/{id}")
    fun deleteService(@PathVariable id: Long): ResponseEntity<Void> {
        serviceService.deleteService(id)
        return ResponseEntity.ok().build()
    }
}
