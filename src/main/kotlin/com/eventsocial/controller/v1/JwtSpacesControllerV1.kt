package com.eventsocial.controller.v1

import com.eventsocial.domain.mappers.SpaceMapper.toSpaceReponse
import com.eventsocial.domain.requests.PatchSpaceRequest
import com.eventsocial.domain.requests.PutSpaceRequest
import com.eventsocial.domain.requests.SpaceRequest
import com.eventsocial.domain.response.SpaceReponse
import com.eventsocial.model.GenericUtils
import com.eventsocial.security.jwt.ITokenExtractor
import com.eventsocial.security.jwt.JwtTokenUtil
import com.eventsocial.services.SpaceService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.data.domain.Page
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.servlet.http.HttpServletRequest

@PreAuthorize("authenticated")
@CrossOrigin
@RestController
@RequestMapping(value = ["v1/space"], produces = [MediaType.APPLICATION_JSON_VALUE])
@Validated
@Api(value = "", tags = ["Estos metodos realizan un ABM de los espacios"], protocols = "https")
class JwtSpacesControllerV1 {

    @Autowired
    lateinit var spaceService: SpaceService

    @Autowired
    @Qualifier("jwtHeaderTokenExtractor")
    lateinit var tokenExtractor: ITokenExtractor

    @PreAuthorize("hasRole('USER')")
    @ApiOperation(value = "")
    @PostMapping
    @ResponseBody
    fun createSpace(
        request: HttpServletRequest,
        @RequestBody(required = true)
        @Validated restaurantRequest: SpaceRequest
    ): ResponseEntity<SpaceReponse> {
        var userCode: UUID? = null

        val tokenPayload = tokenExtractor.Extract(request.getHeader(JwtTokenUtil.TOKEN_HEADER))
        val claims = tokenExtractor.ReadToken(tokenPayload)

        if (!claims.isNullOrEmpty()) {
            if (GenericUtils.getValueFromKeyValue(claims, "userCode") != null) {
                userCode = UUID.fromString(GenericUtils.getValueFromKeyValue(claims, "userCode").toString())
            }
        }
        return ResponseEntity(
            spaceService.createSpace(userCode!!, restaurantRequest).toSpaceReponse(), HttpStatus.CREATED
        )
    }

    @PreAuthorize("hasRole('USER')")
    @ApiOperation(value = "")
    @GetMapping("/{spaceCode}")
    @ResponseBody
    fun getSpaceByCode(
        request: HttpServletRequest,
        @PathVariable("spaceCode", required = true) spaceCode: UUID
    ) = spaceService.getSpaceByCode(spaceCode)?.toSpaceReponse()

    @PreAuthorize("hasRole('USER')")
    @ApiOperation(value = "")
    @PutMapping
    @ResponseBody
    fun putSpace(
        request: HttpServletRequest,
        @RequestBody(required = true)
        @Validated putSpaceRequest: PutSpaceRequest
    ): ResponseEntity<SpaceReponse> {
        var userCode: UUID? = null

        val tokenPayload = tokenExtractor.Extract(request.getHeader(JwtTokenUtil.TOKEN_HEADER))
        val claims = tokenExtractor.ReadToken(tokenPayload)

        if (!claims.isNullOrEmpty()) {
            if (GenericUtils.getValueFromKeyValue(claims, "userCode") != null) {
                userCode = UUID.fromString(GenericUtils.getValueFromKeyValue(claims, "userCode").toString())
            }
        }
        return ResponseEntity(
            spaceService.putSpace(userCode!!, putSpaceRequest).toSpaceReponse(), HttpStatus.CREATED
        )
    }

    @PreAuthorize("hasRole('USER')")
    @ApiOperation(value = "")
    @PatchMapping
    @ResponseBody
    fun patchSpace(
        request: HttpServletRequest,
        @RequestBody(required = true)
        @Validated patchSpaceRequest: PatchSpaceRequest
    ): ResponseEntity<SpaceReponse> {
        var userCode: UUID? = null

        val tokenPayload = tokenExtractor.Extract(request.getHeader(JwtTokenUtil.TOKEN_HEADER))
        val claims = tokenExtractor.ReadToken(tokenPayload)

        if (!claims.isNullOrEmpty()) {
            if (GenericUtils.getValueFromKeyValue(claims, "userCode") != null) {
                userCode = UUID.fromString(GenericUtils.getValueFromKeyValue(claims, "userCode").toString())
            }
        }
        return ResponseEntity(
            spaceService.patchSpace(userCode!!, patchSpaceRequest).toSpaceReponse(), HttpStatus.CREATED
        )
    }

    @PreAuthorize("hasRole('USER')")
    @ApiOperation(value = "")
    @DeleteMapping("/{spaceCode}")
    @ResponseBody
    fun deleteSpace(
        request: HttpServletRequest,
        @PathVariable("spaceCode", required = true) spaceCode: UUID
    ): ResponseEntity<HttpStatus> {
        var userCode: UUID? = null

        val tokenPayload = tokenExtractor.Extract(request.getHeader(JwtTokenUtil.TOKEN_HEADER))
        val claims = tokenExtractor.ReadToken(tokenPayload)

        if (!claims.isNullOrEmpty()) {
            if (GenericUtils.getValueFromKeyValue(claims, "userCode") != null) {
                userCode = UUID.fromString(GenericUtils.getValueFromKeyValue(claims, "userCode").toString())
            }
        }
        spaceService.deleteSpace(userCode!!, spaceCode)

        return ResponseEntity(HttpStatus.OK)
    }

    @PreAuthorize("hasRole('USER')")
    @ApiOperation(value = "")
    @GetMapping
    @ResponseBody
    fun getSpacesByUserCode(
        request: HttpServletRequest,
        @RequestParam("page") page: Int?,
        @RequestParam("offset") offset: Int?
    ): ResponseEntity<Page<SpaceReponse>> {
        var userCode: UUID? = null

        val tokenPayload = tokenExtractor.Extract(request.getHeader(JwtTokenUtil.TOKEN_HEADER))
        val claims = tokenExtractor.ReadToken(tokenPayload)

        if (!claims.isNullOrEmpty()) {
            if (GenericUtils.getValueFromKeyValue(claims, "userCode") != null) {
                userCode = UUID.fromString(GenericUtils.getValueFromKeyValue(claims, "userCode").toString())
            }
        }
        return ResponseEntity(
            spaceService.getSpacesWithPagination(userCode!!, offset ?: 10, page ?: 0)?.map { it.toSpaceReponse() }, HttpStatus.CREATED
        )
    }
}
