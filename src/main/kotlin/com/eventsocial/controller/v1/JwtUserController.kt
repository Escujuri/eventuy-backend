package com.eventsocial.controller.v1

import com.eventsocial.domain.mappers.UsersMapper.toUserReponse
import com.eventsocial.domain.mappers.UsersMapper.toUserSummeryReponse
import com.eventsocial.domain.requests.PatchUserRequest
import com.eventsocial.domain.response.UsersResponse
import com.eventsocial.model.GenericUtils
import com.eventsocial.security.jwt.ITokenExtractor
import com.eventsocial.security.jwt.JwtTokenUtil
import com.eventsocial.services.SpaceService
import com.eventsocial.services.UserService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.servlet.http.HttpServletRequest

@CrossOrigin
@RestController
@RequestMapping(value = ["v1/user"], produces = [MediaType.APPLICATION_JSON_VALUE])
@Validated
@Api(value = "", tags = ["Manejo de informacion de usuarios"], protocols = "https")
class JwtUserController {

    @Autowired
    lateinit var userService: UserService

    @Autowired
    lateinit var spaceService: SpaceService

    @Autowired
    @Qualifier("jwtHeaderTokenExtractor")
    lateinit var tokenExtractor: ITokenExtractor

    @CrossOrigin
    @GetMapping
    @ApiOperation(value = "Recurso que recupera la informacion del usuario by jwt")
    @ResponseBody
    fun getUser(request: HttpServletRequest): ResponseEntity<UsersResponse> {
        var userCode: UUID? = null

        val tokenPayload = tokenExtractor.Extract(request.getHeader(JwtTokenUtil.TOKEN_HEADER))
        val claims = tokenExtractor.ReadToken(tokenPayload)

        if (!claims.isNullOrEmpty()) {
            if (GenericUtils.getValueFromKeyValue(claims, "userCode") != null) {
                userCode = UUID.fromString(GenericUtils.getValueFromKeyValue(claims, "userCode").toString())
            }
        }
        return ResponseEntity(
            userService.getUser(userCode!!).toUserReponse(spaceService.getSpaces(userCode)!!.isNotEmpty()), HttpStatus.CREATED
        )
    }

    @PreAuthorize("hasRole('USER')")
    @ApiOperation(value = "Recurso para cambiar los datos de un usuario")
    @PatchMapping
    @ResponseBody
    fun pathUser(
        request: HttpServletRequest,
        @RequestBody(required = true) patchUserRequest: PatchUserRequest
    ): ResponseEntity<UsersResponse> {
        var userCode: UUID? = null

        val tokenPayload = tokenExtractor.Extract(request.getHeader(JwtTokenUtil.TOKEN_HEADER))
        val claims = tokenExtractor.ReadToken(tokenPayload)

        if (!claims.isNullOrEmpty()) {
            if (GenericUtils.getValueFromKeyValue(claims, "userCode") != null) {
                userCode = UUID.fromString(GenericUtils.getValueFromKeyValue(claims, "userCode").toString())
            }
        }
        return ResponseEntity(
            userService.pathUser(userCode!!, patchUserRequest).toUserReponse(spaceService.getSpaces(userCode)!!.isNotEmpty()), HttpStatus.CREATED
        )
    }

    @CrossOrigin
    @GetMapping("/{userCode}")
    @ApiOperation(value = "Recurso que recupera la informacion del usuario by user code")
    @ResponseBody
    fun getUserByCode(request: HttpServletRequest, @PathVariable("userCode") userCode: UUID) = userService.getUser(userCode).toUserReponse(spaceService.getSpaces(userCode)!!.isNotEmpty())

    @CrossOrigin
    @GetMapping("/all")
    @ApiOperation(value = "Recurso que recupera la informacion de todos los usuarios")
    @ResponseBody
    fun getAllUser(request: HttpServletRequest) = userService.getAllUser().map { it.toUserSummeryReponse() }

    @CrossOrigin
    @GetMapping("/info/chat/list/organizer")
    @ApiOperation(value = "")
    @ResponseBody
    fun getListInfoUserOrganizer(request: HttpServletRequest): Any {
        var userId = ""
        val tokenPayload = tokenExtractor.Extract(request.getHeader(JwtTokenUtil.TOKEN_HEADER))
        val claims = tokenExtractor.ReadToken(tokenPayload)

        if (!claims.isNullOrEmpty()) {
            if (GenericUtils.getValueFromKeyValue(claims, "userId") != null) {
                userId = GenericUtils.getValueFromKeyValue(claims, "userId").toString()
            }
        }
        return ResponseEntity(
            userService.getListInfoUserOrganizer(userId), HttpStatus.OK
        )
    }

    @CrossOrigin
    @GetMapping("/info/chat/list/owner")
    @ApiOperation(value = "")
    @ResponseBody
    fun getListInfoUserOwner(request: HttpServletRequest): Any {

        var userId = ""
        val tokenPayload = tokenExtractor.Extract(request.getHeader(JwtTokenUtil.TOKEN_HEADER))
        val claims = tokenExtractor.ReadToken(tokenPayload)

        if (!claims.isNullOrEmpty()) {
            if (GenericUtils.getValueFromKeyValue(claims, "userId") != null) {
                userId = GenericUtils.getValueFromKeyValue(claims, "userId").toString()
            }
        }
        return ResponseEntity(
            userService.getListInfoUserOwner(userId), HttpStatus.OK
        )
    }
}
