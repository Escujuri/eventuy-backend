package com.eventsocial.controller.v1

import com.eventsocial.security.jwt.JwtRequestStep1
import com.eventsocial.services.RegisterService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.servlet.http.HttpServletRequest

@CrossOrigin
@RestController
@RequestMapping(value = ["v1/register"])
@Validated
@Api(value = "Login", tags = ["Recursos de usuario para el ingreso a la app"], protocols = "https")
class RegisterControllerV1 {

    @Autowired
    lateinit var registerService: RegisterService

    @GetMapping(value = ["/email/{email}"])
    @ApiOperation(value = "Solicita el codigo otp para recuperacion de usuario")
    @ResponseBody
    fun getValidateEmail(
        request: HttpServletRequest?,
        @PathVariable("email") email: String
    ) = registerService.getValidateEmail(email)

    @PostMapping(value = ["/sign-up"])
    @ApiOperation(value = "Recurso para realizar registrarse. Retorno status 200 y json en Body response.")
    @ResponseBody
    fun singUp(
        request: HttpServletRequest,
        @RequestBody(required = true) jwtRequestStep1: JwtRequestStep1
    ) = ResponseEntity(registerService.singUp(jwtRequestStep1), HttpStatus.OK)

    @GetMapping(value = ["/email/{email}/otp"])
    @ApiOperation(value = "Solicita el codigo otp para recuperacion de usuario")
    @ResponseBody
    fun getOTPByEmail(
        request: HttpServletRequest?,
        @PathVariable("email") email: String
    ) = registerService.getOTPEmail(email)

    @GetMapping(value = ["/validate/email/{email}/otp/{otpCode}"])
    @ApiOperation(value = "Valida y cambia la password del usuario")
    @ResponseBody
    fun getValidateOTPByEmailAndCode(
        request: HttpServletRequest?,
        @PathVariable("email") email: String,
        @PathVariable("otpCode") otpCode: String
    ) = registerService.getValidateOTPByEmailAndCode(email, otpCode)
}
