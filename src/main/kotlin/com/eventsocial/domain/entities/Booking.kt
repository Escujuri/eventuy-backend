package com.eventsocial.domain.entities

import com.vladmihalcea.hibernate.type.array.StringArrayType
import com.vladmihalcea.hibernate.type.json.JsonBinaryType
import com.vladmihalcea.hibernate.type.json.JsonStringType
import org.hibernate.annotations.TypeDef
import org.hibernate.annotations.TypeDefs
import java.time.Instant
import java.util.*
import javax.persistence.*

@Entity
@TypeDefs(
    TypeDef(name = "json", typeClass = JsonStringType::class),
    TypeDef(name = "jsonb", typeClass = JsonBinaryType::class),
    TypeDef(name = "array", typeClass = StringArrayType::class)
)
@Table(name = "booking")
open class Booking(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    open var id: Long = 0,

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "users_id", nullable = false)
    open var userId: Users,

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "spaces_id", nullable = false)
    open var spaceId: Spaces,

    @Column(name = "code", nullable = false)
    open var code: UUID = UUID.randomUUID(),

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ownerUserId", nullable = false)
    open var ownerUserId: Users,

    @Column(name = "owner_approval", nullable = false)
    open var ownerApproval: String,

    @Column(name = "url_redirect")
    open var urlRedirect: String,

    @Column(name = "date_check_in")
    open var dateCheckIn: Instant,

    @Column(name = "date_check_out")
    open var dateCheckOut: Instant?,

    @Column(name = "occupancy")
    open var occupancy: Int,

    @Column(name = "amount")
    open var amount: Float,

    @Column(name = "currency")
    open var currency: String,

    @Column(name = "created_at", nullable = false)
    open var createdAt: Instant = Instant.now(),

    @Column(name = "updated_at")
    open var updatedAt: Instant?
)
