package com.eventsocial.domain.entities

import java.util.*
import javax.persistence.*

@Entity
@Table(name = "companies")
data class Company(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Long = 0,

    var serviceCode: UUID = UUID.randomUUID(),

    @ElementCollection
    @CollectionTable(name = "service_categories")
    @Column(name = "category")
    var serviceCategoryObject: List<String> = emptyList(),

    var title: String = "",

    var description: String = "",

    var image: String = "",

    @Embedded
    var address: Address = Address(),

    var withShipping: Boolean = false,

    @ElementCollection
    @CollectionTable(name = "distance_prices")
    var distanceXprice: List<DistancePrice> = emptyList(),

    var antelationNumber: Int = 0,

    var antelationTime: String = "",

    @ElementCollection
    @CollectionTable(name = "shipment_days")
    @Column(name = "day")
    var shipmentDays: List<String> = emptyList(),

    var shipmentHourStart: String = "",

    var shipmentHourEnd: String = "",

    var freeShipping: Boolean = false,

    var freeShippingAfterPrice: Int = 0,

    var freeShippingMaxDistance: Int = 0,

    @OneToMany(cascade = [CascadeType.ALL])
    @JoinColumn(name = "company_id")
    var productList: List<Product> = emptyList(),

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "users_id", nullable = true)
    var userId: Users,

    var cancellationsType: String = "",

    @Column(name = "ranking")
    var ranking: Float = 0F,

    @Column(name = "ranking_amount")
    var rankingAmount: Int? = 0,
)

@Embeddable
data class Address(
    var number: Int = 0,
    var street: String = "",
    var location: String = "",
    var city: String = "",
    var province: String = "",
    var country: String = "",
    var cp: String = ""
)

@Embeddable
data class DistancePrice(
    var distanceStart: Int = 0,
    var distanceEnd: Int = 0,
    var price: Int = 0
)
