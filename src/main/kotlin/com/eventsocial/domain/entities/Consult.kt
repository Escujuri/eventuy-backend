package com.eventsocial.domain.entities

import java.time.Instant
import javax.persistence.*

@NamedNativeQueries
@Entity
@Table(name = "Consult")
open class Consult(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    open var id: Long = 0,

    @Column(name = "nombre")
    open var nombre: String? = null,

    @Column(name = "email")
    open var email: String? = null,

    @Column(name = "asunto")
    open var asunto: String? = null,

    @Column(name = "mensaje")
    open var mensaje: String? = null,

    @Column(name = "ip")
    open var ip: String? = null,

    @Column(name = "fecha_creacion")
    open var created_at: Instant = Instant.now(),

    @Column(name = "code")
    open var code: String? = null,
)
