package com.eventsocial.domain.entities

import com.vladmihalcea.hibernate.type.array.StringArrayType
import com.vladmihalcea.hibernate.type.json.JsonBinaryType
import com.vladmihalcea.hibernate.type.json.JsonStringType
import org.hibernate.annotations.TypeDef
import org.hibernate.annotations.TypeDefs
import java.time.Instant
import java.util.*
import javax.persistence.*

@Entity
@TypeDefs(
    TypeDef(name = "json", typeClass = JsonStringType::class),
    TypeDef(name = "jsonb", typeClass = JsonBinaryType::class),
    TypeDef(name = "array", typeClass = StringArrayType::class)
)
@Table(name = "user_order")
data class Order(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    open var id: Long = 0,

    @Column(name = "total_price", nullable = false)
    open var totalPrice: Double,

    @Column(name = "currency", nullable = false)
    open var currency: String,

    @Column(name = "code", nullable = false)
    open var code: UUID = UUID.randomUUID(),

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "users_id", nullable = false)
    open var user: Users,

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ownerUser", nullable = false)
    open var ownerUser: Users,

    @Column(name = "owner_approval", nullable = false)
    open var ownerApproval: String,

    @Column(name = "url_redirect")
    open var urlRedirect: String,

    @Column(name = "order_date_at", nullable = false)
    open var orderDateAt: Date,

    @Column(name = "created_at", nullable = false)
    open var createdAt: Instant = Instant.now(),

    @Column(name = "updated_at")
    open var updatedAt: Instant? = null,

    @Column(name = "street", nullable = false)
    open var street: String,

    @Column(name = "number", nullable = false)
    open var number: Int,

    @Column(name = "location", nullable = false)
    open var location: String,

    @Column(name = "city", nullable = false)
    open var city: String,

    @Column(name = "province", nullable = false)
    open var province: String,

    @Column(name = "country", nullable = false)
    open var country: String,

    @Column(name = "cp", nullable = false)
    open var cp: String,
)
