package com.eventsocial.domain.entities

import java.util.UUID
import javax.persistence.*

@Entity
@Table(name = "products")
data class Product(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = 0,

    var code: UUID = UUID.randomUUID(),

    var title: String = "",

    var description: String = "",

    @ElementCollection
    @CollectionTable(name = "product_images")
    @Column(name = "image")
    var images: List<String> = emptyList(),

    var serviceType: String = "",

    @ElementCollection
    @CollectionTable(name = "product_types")
    @Column(name = "type")
    var productTypes: List<String> = emptyList(),

    var readyToEat: Boolean = false,

    var price: Int = 0,

    var prepDescription: String = "",

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "company_id", nullable = false)
    val company: Company
)
