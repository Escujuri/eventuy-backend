package com.eventsocial.domain.entities

import java.time.Instant
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "register")
open class Register(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    open var id: Long = 0,

    @Column(name = "email", nullable = false)
    open var email: String,

    @Column(name = "password")
    open var password: String? = null,

    @Column(name = "validated", nullable = false)
    open var validated: Boolean = false,

    @Column(name = "created_at", nullable = false)
    open var created_at: Instant = Instant.now(),

    @Column(name = "code", nullable = false)
    open var code: UUID = UUID.randomUUID(),

    @Column(name = "otp_code", nullable = true)
    open var otpCode: String? = null,

    @Column(name = "date_validation_otp", nullable = true)
    open var dateValidationOTP: Instant? = null

)
