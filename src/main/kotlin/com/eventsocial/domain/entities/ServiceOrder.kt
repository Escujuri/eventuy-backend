package com.eventsocial.domain.entities

import javax.persistence.*

@Entity
@Table(name = "Service_Order")
data class ServiceOrder(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long = 0,

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "service_id", nullable = false)
    val services: Services,

    @Column(name = "quantity", nullable = false)
    val quantity: Int,

    @Column(name = "total_price", nullable = false)
    val totalPrice: Double,

)
