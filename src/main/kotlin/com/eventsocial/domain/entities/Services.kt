package com.eventsocial.domain.entities

import javax.persistence.*

@Entity
@Table(name = "Service")
data class Services(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    open var id: Long = 0,

    @Column(name = "name", nullable = false)
    open var name: String,

    @Column(name = "description")
    open var description: String? = null,

)
