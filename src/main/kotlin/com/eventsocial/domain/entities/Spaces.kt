package com.eventsocial.domain.entities

import com.eventsocial.domain.requests.OpenHoursModel
import com.vladmihalcea.hibernate.type.array.StringArrayType
import com.vladmihalcea.hibernate.type.json.JsonBinaryType
import org.hibernate.annotations.Type
import org.hibernate.annotations.TypeDef
import org.hibernate.annotations.TypeDefs
import java.math.BigDecimal
import java.time.Instant
import java.util.*
import javax.persistence.*

@Entity
@TypeDefs(
    TypeDef(name = "jsonb", typeClass = JsonBinaryType::class),
    TypeDef(name = "array", typeClass = StringArrayType::class)
)
@Table(name = "spaces")
open class Spaces(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    open var id: Long = 0,

    @Column(name = "user_code", nullable = false)
    open var userCode: UUID,

    @Column(name = "code", nullable = false)
    open var code: UUID = UUID.randomUUID(),

    @Column(name = "title", nullable = false)
    open var title: String,

    @Column(name = "space_type", nullable = false)
    open var spaceType: String,

    @Column(name = "latitud")
    open var latitud: String? = null,

    @Column(name = "longitud")
    open var longitud: String? = null,

    @Column(name = "street", nullable = false)
    open var street: String,

    @Column(name = "cp", nullable = false)
    open var cp: String,

    @Column(name = "number", nullable = false)
    open var number: Int,

    @Column(name = "location", nullable = false)
    open var location: String,

    @Column(name = "city", nullable = false)
    open var city: String,

    @Column(name = "province", nullable = false)
    open var province: String,

    @Column(name = "country", nullable = false)
    open var country: String,

    @Column(name = "description")
    open var description: String?,

    @Column(name = "price", nullable = false)
    open var price: BigDecimal,

    @Column(name = "clean_price")
    open var cleanPrice: BigDecimal?,

    @Column(name = "ranking", nullable = false)
    open var ranking: Float = 0F,

    @Column(name = "ranking_amount")
    open var rankingAmount: Int? = 0,

    @Column(name = "maximum_occupancy")
    open var maximumOccupancy: Int,

    @Column(name = "hours_availability")
    @Type(type = "array")
    open var hoursAvailability: Array<String>?,

    @Column(name = "publishedDate", nullable = false)
    open var publishedDate: Instant = Instant.now(),

    @Column(name = "minimum_reservation_hours")
    open var minimumReservationHours: Int?,

    @Column(name = "over_night_allowed")
    open var overNightAllowed: Boolean?,

    @Column(name = "price_type")
    open var priceType: Boolean?, // TODO ENUM

    @Column(name = "publish")
    open var publish: Boolean?,

    @Column(name = "dates_not_available")
    @Type(type = "array")
    open var datesNotAvailable: Array<String>?,

    @Column(name = "name_contact")
    open var nameContact: String?,

    @Column(name = "last_name_contact")
    open var lastNameContact: String?,

    @Column(name = "telephone_contact")
    open var telephoneContact: String?,

    @Column(name = "cancellationsType")
    open var cancellationsType: String?,

    @Column(name = "enableSpace")
    open var enableSpace: Boolean?,

    @Type(type = "jsonb")
    @Column(name = "open_hours")
    open var openHours: OpenHoursModel?,

    @Column(name = "images")
    @Type(type = "array")
    open var images: Array<String>?,

    @Column(name = "services")
    @Type(type = "array")
    open var services: Array<String>?,

    @Column(name = "security_services")
    @Type(type = "array")
    open var securityService: Array<String>?,

    @Column(name = "created_at", nullable = false)
    open var createdAt: Instant = Instant.now(),

    @Column(name = "updated_at")
    open var updatedAt: Instant?
)
