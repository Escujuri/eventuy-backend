package com.eventsocial.domain.entities

import java.time.Instant
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "Transactional")
open class Transactional(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    open var id: Long = 0,

    @Column(name = "idempotency_id", nullable = false)
    open var idempotencyId: String,

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "booking_id")
    open var bookingId: Booking? = null,

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_order_id")
    open var orderId: Order? = null,

    @Column(name = "request_id")
    open var requestId: String? = null,

    @Column(name = "payment_at")
    open var paymentAt: Instant? = null,

    @Column(name = "payment_status")
    open var paymentStatus: String? = null,

    @Column(name = "payment_method")
    open var paymentMethod: String? = null,

    @Column(name = "amount")
    open var amount: Float,

    @Column(name = "currency")
    open var currency: String,

    @Column(name = "collector_id")
    open var collectorId: String? = null,

    @Column(name = "approved_payment")
    open var approvedPayment: Instant? = null,

    @Column(name = "montoNetoRecibido")
    open var montoNetoRecibido: Float? = null,

    @Column(name = "payment_id")
    open var paymentId: String? = null,

    @Column(name = "payment_type")
    open var paymentType: String? = null,

    @Column(name = "payment_type_id")
    open var paymentTypeId: String? = null,

    @Column(name = "code", nullable = false)
    open var code: UUID = UUID.randomUUID(),

    @Column(name = "created_at", nullable = false)
    open var createdAt: Instant = Instant.now(),

    @Column(name = "updated_at")
    open var updatedAt: Instant?
)
