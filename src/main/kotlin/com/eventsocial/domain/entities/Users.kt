package com.eventsocial.domain.entities

import com.vladmihalcea.hibernate.type.array.StringArrayType
import org.hibernate.annotations.Type
import org.hibernate.annotations.TypeDef
import java.time.Instant
import java.util.*
import javax.persistence.*

@Entity
@TypeDef(name = "array", typeClass = StringArrayType::class)
@Table(name = "users")
open class Users(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    open var id: Long = 0,

    @Column(name = "nick_name")
    open var nickName: String? = null,

    @Column(name = "name")
    open var name: String? = null,

    @Column(name = "last_name")
    open var lastName: String? = null,

    @Column(name = "age")
    open var age: Int? = null,

    @Column(name = "country_phone_code")
    open var countryPhoneCode: String? = null,

    @Column(name = "phone")
    open var phone: String? = null,

    @Column(name = "code", nullable = false)
    open var code: UUID = UUID.randomUUID(),

    @Column(name = "icon")
    open var icon: String? = null,

    @Column(name = "documento")
    open var documento: String? = null,

    @Column(name = "favorites")
    @Type(type = "array")
    open var favorites: Array<String>?,

    @Column(name = "birthDate")
    open var birthDate: Date? = null,

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "register_id")
    open var registerId: Register,

    @Column(name = "created_at", nullable = false)
    open var createdAt: Instant = Instant.now(),

    @Column(name = "updated_at")
    open var updatedAt: Instant?
)
