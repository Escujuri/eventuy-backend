package com.eventsocial.domain.mappers

import com.eventsocial.domain.entities.Booking
import com.eventsocial.domain.requests.BookingDateResponse
import com.eventsocial.domain.response.BookingResponse
import com.eventsocial.services.TransactionalService
import java.time.ZoneId
import java.time.format.DateTimeFormatter
object BookignMapper {

    fun Booking.toBookingReponse(paymentStatus: TransactionalService.StatusPayment): BookingResponse {
        return BookingResponse(
            date = BookingDateResponse(
                dateCheckIn = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'").format(dateCheckIn.atZone(ZoneId.systemDefault())),
                dateCheckOut = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'").format(dateCheckOut?.atZone(ZoneId.systemDefault()))
            ),
            code = code,
            ownerApproval = ownerApproval,
            ownerCode = ownerUserId.code,
            userCode = userId.code,
            spaceCode = spaceId.code,
            occupancy = occupancy,
            amount = amount,
            currency = currency,
            paymentStatus = paymentStatus
        )
    }
}
