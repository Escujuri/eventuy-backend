package com.eventsocial.domain.mappers

import com.eventsocial.domain.entities.Company
import com.eventsocial.domain.response.CompanyReponse
import com.eventsocial.domain.response.CompanySumaryReponse
import com.eventsocial.domain.response.ProductDTO

object CompanyMapper {
    fun Company.toCompanyReponse(): CompanyReponse {
        return CompanyReponse(
            id = id,
            serviceCode = serviceCode,
            serviceCategoryObject = serviceCategoryObject,
            title = title,
            description = description,
            image = image,
            address = address,
            withShipping = withShipping,
            distanceXprice = distanceXprice,
            antelationNumber = antelationNumber,
            antelationTime = antelationTime,
            shipmentDays = shipmentDays,
            shipmentHourStart = shipmentHourStart,
            shipmentHourEnd = shipmentHourEnd,
            freeShipping = freeShipping,
            freeShippingAfterPrice = freeShippingAfterPrice,
            freeShippingMaxDistance = freeShippingMaxDistance,
            productList = productList.map {
                ProductDTO(
                    id = it.id,
                    code = it.code,
                    title = it.title,
                    description = it.description,
                    images = it.images,
                    serviceType = it.serviceType,
                    productTypes = it.productTypes,
                    readyToEat = it.readyToEat,
                    price = it.price,
                    prepDescription = it.prepDescription
                )
            },
            userCode = userId.code,
            cancellationsType = cancellationsType,
            ranking = ranking,
            rankingAmount = rankingAmount
        )
    }
}

object CompanySumaryMapper {
    fun Company.toCompanySumaryReponse(): CompanySumaryReponse {
        return CompanySumaryReponse(
            id = id,
            serviceCode = serviceCode,
            serviceCategoryObject = serviceCategoryObject,
            title = title,
            description = description,
            image = image,
            address = address,
            withShipping = withShipping,
            distanceXprice = distanceXprice,
            antelationNumber = antelationNumber,
            antelationTime = antelationTime,
            shipmentDays = shipmentDays,
            shipmentHourStart = shipmentHourStart,
            shipmentHourEnd = shipmentHourEnd,
            freeShipping = freeShipping,
            freeShippingAfterPrice = freeShippingAfterPrice,
            freeShippingMaxDistance = freeShippingMaxDistance,
            productList = productList.map {
                ProductDTO(
                    id = it.id,
                    code = it.code,
                    title = it.title,
                    description = it.description,
                    images = it.images,
                    serviceType = it.serviceType,
                    productTypes = it.productTypes,
                    readyToEat = it.readyToEat,
                    price = it.price,
                    prepDescription = it.prepDescription
                )
            },
            userCode = userId.code,
            cancellationsType = cancellationsType,
            ranking = ranking,
            rankingAmount = rankingAmount
        )
    }
}
