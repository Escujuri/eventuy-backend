package com.eventsocial.domain.mappers

import com.eventsocial.domain.entities.Order
import com.eventsocial.domain.entities.ProductOrder
import com.eventsocial.domain.response.OrderResponse
import com.eventsocial.domain.response.OrderSummaryResponse
import com.eventsocial.domain.response.ProductSumaryDTO
import com.eventsocial.services.TransactionalService

object OrderMapper {
    fun Order.toOrderResponse(paymentStatus: TransactionalService.StatusPayment): OrderResponse {
        return OrderResponse(
            totalPrice = totalPrice,
            currency = currency,
            orderCode = code,
            user = user,
            ownerUser = ownerUser,
            ownerApproval = ownerApproval,
            urlRedirect = urlRedirect,
            createdAt = createdAt,
            updatedAt = updatedAt,
            street = street,
            number = number,
            location = location,
            city = city,
            province = province,
            country = country,
            cp = cp,
            paymentStatus = paymentStatus,
            orderDateAt = orderDateAt,
        )
    }

    fun Order.toOrderSummaryResponse(paymentStatus: TransactionalService.StatusPayment, orderProduct: List<ProductOrder>): OrderSummaryResponse {
        return OrderSummaryResponse(
            totalPrice = totalPrice,
            currency = currency,
            orderCode = code,
            userCode = user.code,
            ownerUserCode = ownerUser.code,
            ownerApproval = ownerApproval,
            urlRedirect = urlRedirect,
            createdAt = createdAt,
            updatedAt = updatedAt,
            street = street,
            number = number,
            location = location,
            city = city,
            province = province,
            country = country,
            cp = cp,
            paymentStatus = paymentStatus,
            orderDateAt = orderDateAt,
            companyCode = orderProduct.map { it.product.company.serviceCode }.first(),
            products = orderProduct.map {
                ProductSumaryDTO(
                    id = it.product.id,
                    code = it.product.code,
                    title = it.product.title,
                    description = it.product.description,
                    images = it.product.images,
                    serviceType = it.product.serviceType,
                    productTypes = it.product.productTypes,
                    readyToEat = it.product.readyToEat,
                    price = it.product.price,
                    prepDescription = it.product.prepDescription,
                )
            }
        )
    }
}
