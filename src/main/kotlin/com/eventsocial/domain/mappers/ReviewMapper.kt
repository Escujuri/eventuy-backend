package com.eventsocial.domain.mappers

import com.eventsocial.domain.entities.Reviews
import com.eventsocial.domain.entities.ReviewsCompanies
import com.eventsocial.domain.response.ReviewCompanyReponse
import com.eventsocial.domain.response.ReviewSpaceReponse

object ReviewMapper {
    fun ReviewsCompanies.toReviewCompanyReponse(): ReviewCompanyReponse {
        return ReviewCompanyReponse(
            userCode = userCode,
            companyCode = companyId!!.serviceCode,
            rating = rating,
            comment = comment,
            description = description,
            image = image,
            userName = userName
        )
    }

    fun Reviews.toReviewSpaceReponse(): ReviewSpaceReponse {
        return ReviewSpaceReponse(
            userCode = userCode,
            serviceCode = spaceId!!.code,
            rating = rating,
            comment = comment,
            description = description,
            image = image,
            userName = userName
        )
    }
}
