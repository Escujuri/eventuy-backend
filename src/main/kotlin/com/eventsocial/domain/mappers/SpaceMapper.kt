package com.eventsocial.domain.mappers

import com.eventsocial.domain.entities.Spaces
import com.eventsocial.domain.requests.Adress
import com.eventsocial.domain.response.FilterSpacesReponse
import com.eventsocial.domain.response.SpaceReponse

object SpaceMapper {
    fun Spaces.toSpaceReponse(): SpaceReponse {
        return SpaceReponse(
            userCode = userCode,
            spaceCode = code,
            title = title,
            spaceType = spaceType,
            adress = Adress(
                street = street,
                number = number,
                location = location,
                city = city,
                province = province,
                country = country,
                cp = cp
            ),
            description = description,
            price = price,
            services = services,
            securityService = securityService,
            maximumOccupancy = maximumOccupancy,
            hoursAvailability = hoursAvailability,
            priceType = priceType,
            publish = publish,
            datesNotAvailable = datesNotAvailable,
            nameContact = nameContact,
            lastNameContact = lastNameContact,
            telephoneContact = telephoneContact,
            cancellationsType = cancellationsType,
            openHours = openHours,
            images = images
        )
    }

    fun Spaces.toFilterSpacesReponse(): FilterSpacesReponse {
        return FilterSpacesReponse(
            title = title,
            spaceType = spaceType,
            images = images,
            services = services,
            spaceCode = code,
            ranking = ranking,
            price = price,
            rankingAmount = rankingAmount,
            maximumOccupancy = maximumOccupancy,
            address = Adress(
                street = street,
                number = number,
                location = location,
                city = city,
                province = province,
                country = country,
                cp = cp
            )
        )
    }
}
