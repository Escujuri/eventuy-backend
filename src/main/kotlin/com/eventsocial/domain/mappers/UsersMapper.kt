package com.eventsocial.domain.mappers

import com.eventsocial.domain.entities.Users
import com.eventsocial.domain.response.UsersResponse
import com.eventsocial.domain.response.UsersSummaryResponse

object UsersMapper {
    fun Users.toUserReponse(owner: Boolean): UsersResponse {
        return UsersResponse(
            name = name,
            lastName = lastName,
            age = age,
            phone = phone,
            code = code,
            icon = icon,
            birthDate = birthDate,
            documento = documento,
            favorites = favorites,
            owner = owner,
            email = registerId.email
        )
    }

    fun Users.toUserSummeryReponse(): UsersSummaryResponse {
        return UsersSummaryResponse(
            name = name,
            lastName = lastName,
            age = age,
            phone = phone,
            code = code,
            icon = icon,
            birthDate = birthDate,
            documento = documento,
        )
    }
}
