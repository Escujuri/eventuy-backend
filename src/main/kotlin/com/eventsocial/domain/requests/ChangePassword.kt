package com.eventsocial.domain.requests

data class ChangePassword(
    val email: String,
    val oldPassword: String,
    val newPassword: String,
)
