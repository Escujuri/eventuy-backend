package com.eventsocial.domain.requests

data class ChangeStatusBookingRequest(
    val urlRedirect: String
)
