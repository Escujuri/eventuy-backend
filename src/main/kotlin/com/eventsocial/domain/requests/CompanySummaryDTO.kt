package com.eventsocial.domain.requests

import com.eventsocial.domain.entities.Address
import java.util.*

data class CompanySummaryDTO(
    val serviceCategoryObject: List<String>,
    val title: String,
    val description: String,
    val withShipping: Boolean,
    val serviceCode: UUID,
    val freeShipping: Boolean,
    val address: Address,
    val image: String,
    val ranking: Float,
    val rankingAmount: Int?,
)
