package com.eventsocial.domain.requests

import java.time.Instant
import java.util.*

data class CreateBooking(
    val date: BookingDate,
    val spaceCode: UUID,
    val occupancy: Int,
    val urlRedirect: String,
    val amount: Float,
    val currency: String
)

data class BookingDate(
    val dateCheckIn: Instant,
    val dateCheckOut: Instant?,
)

data class BookingDateResponse(
    val dateCheckIn: String,
    val dateCheckOut: String?,
)
