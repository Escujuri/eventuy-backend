package com.eventsocial.domain.requests

import java.util.*

data class CreateBookingPaymentStripe(
    val bookingCode: UUID,
    val urlBack: String
)
