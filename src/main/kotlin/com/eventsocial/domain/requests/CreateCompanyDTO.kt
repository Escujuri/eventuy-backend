package com.eventsocial.domain.requests

import com.eventsocial.domain.entities.Address
import com.eventsocial.domain.entities.DistancePrice
import com.eventsocial.domain.entities.Product
import java.util.*

data class CreateCompanyDTO(
    var serviceCode: UUID = UUID.randomUUID(),
    var serviceCategoryObject: List<String> = emptyList(),
    var title: String = "",
    var description: String = "",
    var image: String = "",
    var address: Address,
    var withShipping: Boolean = false,
    var distanceXprice: List<DistancePrice> = emptyList(),
    var antelationNumber: Int = 0,
    var antelationTime: String = "",
    var shipmentDays: List<String> = emptyList(),
    var shipmentHourStart: String = "",
    var shipmentHourEnd: String = "",
    var freeShipping: Boolean = false,
    var freeShippingAfterPrice: Int = 0,
    var freeShippingMaxDistance: Int = 0,
    var productList: List<Product> = emptyList(),
    var cancellationsType: String = "",
    var ranking: Float = 0F,
    var rankingAmount: Int? = 0
)
