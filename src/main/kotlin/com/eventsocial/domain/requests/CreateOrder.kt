package com.eventsocial.domain.requests

import java.util.*

data class CreateOrder(
    val urlRedirect: String,
    val totalPrice: Double,
    val orderDateAt: Date,
    val currency: String,
    val address: Adress,
    val order: Order
)

data class Order(
    val companyCode: UUID,
    val products: List<Products>
)

data class Products(
    val productId: Long,
    val quantity: Int,
    val totalPrice: Double
)
