package com.eventsocial.domain.requests

import java.util.*

data class CreateOrderPaymentStripe(
    val orderCode: UUID,
    val urlBack: String
)
