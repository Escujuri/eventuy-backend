package com.eventsocial.domain.requests

import java.util.*

data class CreatePaymentMP(
    val bookingCode: UUID,
    val urlBack: String
)
