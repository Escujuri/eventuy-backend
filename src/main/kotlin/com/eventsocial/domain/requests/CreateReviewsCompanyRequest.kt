package com.eventsocial.domain.requests

import com.eventsocial.domain.entities.Company
import com.eventsocial.domain.entities.ReviewsCompanies
import java.util.*

data class CreateReviewsCompanyRequest(
    val companyCode: UUID,
    val rating: Int,
    val comment: String,
    val description: String?,
    val image: String?,
    val username: String?
) {
    fun toModel(userCode: UUID, company: Company) = ReviewsCompanies(
        userCode = userCode,
        companyId = company,
        rating = rating,
        comment = comment,
        description = description,
        image = image,
        userName = username
    )
}
