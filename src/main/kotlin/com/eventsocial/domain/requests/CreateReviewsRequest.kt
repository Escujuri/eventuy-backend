package com.eventsocial.domain.requests

import com.eventsocial.domain.entities.Reviews
import com.eventsocial.domain.entities.Spaces
import java.util.*

data class CreateReviewsRequest(
    val spaceCode: UUID,
    val rating: Int,
    val comment: String,
    val description: String?,
    val image: String?,
    val username: String?
) {
    fun toModel(userCode: UUID, space: Spaces) = Reviews(
        userCode = userCode,
        spaceId = space,
        rating = rating,
        comment = comment,
        description = description,
        image = image,
        userName = username
    )
}
