package com.eventsocial.domain.requests

import java.util.*

data class PatchUserRequest(
    val name: String?,
    val lastName: String?,
    val age: Int?,
    val phone: String?,
    val birthDate: Date?,
    val documento: String?,
    val icon: String?,
    val favorites: List<String>?
)
