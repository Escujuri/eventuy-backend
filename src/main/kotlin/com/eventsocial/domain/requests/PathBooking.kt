package com.eventsocial.domain.requests

import java.math.BigDecimal
import java.util.*

data class PathBooking(
    val date: BookingDate,
    val spaceCode: UUID,
    val occupancy: Int,
    val amount: BigDecimal,
)
