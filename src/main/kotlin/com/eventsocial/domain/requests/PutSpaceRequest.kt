package com.eventsocial.domain.requests

import java.math.BigDecimal

data class PutSpaceRequest(
    val title: String,
    val typeSpace: SpacesTypes,
    val adress: Adress,
    val description: String?,
    val price: BigDecimal,
    val cleanPrice: BigDecimal?,
    val maximumOccupancy: Int,
    val hoursAvailability: Array<String>?,
    val services: Array<String>?,
    val securityService: Array<String>?,
    val images: Array<String>?,
    val priceType: Boolean?,
    val publish: Boolean?,
    val datesNotAvailable: Array<String>?,
    val nameContact: String?,
    val lastNameContact: String?,
    val telephoneContact: String?,
    val openHours: OpenHoursModel?,
    val cancellationsType: String?,
    val minimumReservationHours: Int?,
    val overNightAllowed: Boolean?
)
