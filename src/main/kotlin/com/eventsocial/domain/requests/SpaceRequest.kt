package com.eventsocial.domain.requests

import com.eventsocial.domain.entities.Spaces
import java.math.BigDecimal
import java.util.*

data class SpaceRequest(
    val title: String,
    val spaceType: SpacesTypes,
    val adress: Adress,
    val description: String?,
    val price: BigDecimal,
    val cleanPrice: BigDecimal?,
    val maximumOccupancy: Int,
    val hoursAvailability: Array<String>?,
    val minimumReservationHours: Int?,
    val overNightAllowed: Boolean,
    val services: Array<String>?,
    val securityService: Array<String>?,
    val images: Array<String>?,
    val priceType: Boolean?,
    val publish: Boolean?,
    val datesNotAvailable: Array<String>?,
    val nameContact: String?,
    val lastNameContact: String?,
    val telephoneContact: String?,
    val openHours: OpenHoursModel?,
    val cancellationsType: String?,
    val rankingAmount: Int
) {
    fun toModel(userCode: UUID) = Spaces(
        userCode = userCode,
        title = title,
        spaceType = spaceType.name,
        street = adress.street,
        cp = adress.cp,
        number = adress.number,
        location = adress.location,
        city = adress.city,
        province = adress.province,
        country = adress.country,
        description = description,
        price = price,
        services = services,
        securityService = securityService,
        maximumOccupancy = maximumOccupancy,
        hoursAvailability = hoursAvailability,
        priceType = priceType,
        publish = publish,
        datesNotAvailable = datesNotAvailable,
        nameContact = nameContact,
        lastNameContact = lastNameContact,
        telephoneContact = telephoneContact,
        cancellationsType = cancellationsType,
        openHours = openHours,
        images = images,
        cleanPrice = cleanPrice,
        rankingAmount = rankingAmount,
        minimumReservationHours = minimumReservationHours,
        overNightAllowed = overNightAllowed,
        enableSpace = true,
        updatedAt = null
    )
}

data class OpenHoursModel(
    val monday: OpenHoursDayModel?,
    val tuesday: OpenHoursDayModel?,
    val wednesday: OpenHoursDayModel?,
    val thursday: OpenHoursDayModel?,
    val friday: OpenHoursDayModel?,
    val saturday: OpenHoursDayModel?,
    val sunday: OpenHoursDayModel?,
)

data class OpenHoursDayModel(
    val openHours: String?,
    val closeHours: String?,
    val isOpen: Boolean,
)

data class Adress(
    val street: String,
    val number: Int,
    val location: String,
    val city: String,
    val province: String,
    val country: String,
    val cp: String,
)

enum class SpacesTypes {
    BAR, RESTORANTE, QUINTA, SALON, MASIA, CASA_RURAL
}
