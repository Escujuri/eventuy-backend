package com.eventsocial.domain.requests

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

data class StatusChangePaymentMP(
    @JsonProperty("action")
    val action: String?,
    @JsonProperty("id")
    val paymentId: String?,
    @JsonProperty("updateAt")
    val updateAt: Date?,
    @JsonProperty("type")
    val type: String?,
    @JsonProperty("resource")
    val resource: String?,
    @JsonProperty("topic")
    val topic: String?
)
