package com.eventsocial.domain.requests

import com.eventsocial.domain.entities.Address
import com.eventsocial.domain.entities.DistancePrice
import com.eventsocial.domain.entities.Product
import java.util.*

data class UpdateCompanyDTO(
    var serviceCategoryObject: List<String>?,
    var title: String?,
    var description: String?,
    var image: String?,
    var address: Address?,
    var withShipping: Boolean?,
    var distanceXprice: List<DistancePrice>?,
    var antelationNumber: Int?,
    var antelationTime: String?,
    var shipmentDays: List<String>?,
    var shipmentHourStart: String?,
    var shipmentHourEnd: String?,
    var freeShipping: Boolean?,
    var freeShippingAfterPrice: Int?,
    var freeShippingMaxDistance: Int?,
    var productList: List<Product>?,
    var cancellationsType: String?,
)
