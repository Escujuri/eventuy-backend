package com.eventsocial.domain.requests

import com.fasterxml.jackson.annotation.JsonProperty
import java.math.BigDecimal
import java.time.Instant

data class WebhookPaymentStripe(
    val id: String,
    @JsonProperty("object")
    val _object: String,
    val api_version: String,
    val created: Instant,
    val data: Object,
    val livemode: Boolean,
    val pending_webhooks: Int,
    val request: RequestIdinfo,
    val type: String
)

data class RequestIdinfo(
    val id: String,
    val idempotency_key: String
)

data class Object(
    @JsonProperty("object")
    val _object: ObjectData
)

data class ObjectData(
    val id: String,
    @JsonProperty("object")
    val _object: String,
    val amount: BigDecimal,
    val amount_capturable: BigDecimal?,
    val amount_received: BigDecimal?,
    val application_fee_amount: BigDecimal?,
    val currency: String,
    val customer: String?,
    val description: String
)
