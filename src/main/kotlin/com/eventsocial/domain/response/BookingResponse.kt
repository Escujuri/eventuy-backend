package com.eventsocial.domain.response

import com.eventsocial.domain.requests.BookingDateResponse
import com.eventsocial.services.TransactionalService
import java.util.*

data class BookingResponse(
    val date: BookingDateResponse,
    val code: UUID,
    val spaceCode: UUID,
    val ownerCode: UUID,
    val userCode: UUID,
    val ownerApproval: String,
    val occupancy: Int,
    val amount: Float,
    val currency: String,
    val paymentStatus: TransactionalService.StatusPayment
)
