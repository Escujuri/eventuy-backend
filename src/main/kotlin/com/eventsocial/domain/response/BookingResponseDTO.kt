package com.eventsocial.domain.response

import com.eventsocial.domain.entities.Transactional
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonPropertyOrder

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder("code", "message")
class BookingResponseDTO {
    /*---- GETTER SETTER----*/
    @JsonProperty("code")
    var code: String? = null

    @JsonProperty("reservas")
    var bookings: List<Transactional>? = null

    @JsonProperty("message")
    var message: String? = null
}
