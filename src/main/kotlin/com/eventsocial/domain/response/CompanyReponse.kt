package com.eventsocial.domain.response

import com.eventsocial.domain.entities.Address
import com.eventsocial.domain.entities.DistancePrice
import java.util.*

data class CompanyReponse(
    var id: Long = 0,
    var serviceCode: UUID = UUID.randomUUID(),
    var serviceCategoryObject: List<String> = emptyList(),
    var title: String = "",
    var description: String = "",
    var image: String = "",
    var address: Address,
    var withShipping: Boolean = false,
    var distanceXprice: List<DistancePrice> = emptyList(),
    var antelationNumber: Int = 0,
    var antelationTime: String = "",
    var shipmentDays: List<String> = emptyList(),
    var shipmentHourStart: String = "",
    var shipmentHourEnd: String = "",
    var freeShipping: Boolean = false,
    var freeShippingAfterPrice: Int = 0,
    var freeShippingMaxDistance: Int = 0,
    var productList: List<ProductDTO>,
    var userCode: UUID,
    var cancellationsType: String = "",
    var ranking: Float = 0F,
    var rankingAmount: Int? = 0
)

data class CompanySumaryReponse(
    var id: Long = 0,
    var serviceCode: UUID,
    var serviceCategoryObject: List<String>,
    var title: String,
    var description: String,
    var image: String,
    var address: Address,
    var withShipping: Boolean,
    var distanceXprice: List<DistancePrice>,
    var antelationNumber: Int,
    var antelationTime: String,
    var shipmentDays: List<String>,
    var shipmentHourStart: String,
    var shipmentHourEnd: String,
    var freeShipping: Boolean,
    var freeShippingAfterPrice: Int,
    var freeShippingMaxDistance: Int,
    var productList: List<ProductDTO>,
    var userCode: UUID,
    var cancellationsType: String,
    var ranking: Float,
    var rankingAmount: Int?
)

data class ProductDTO(
    var id: Long?,
    var code: UUID?,
    var title: String?,
    var description: String?,
    var images: List<String>?,
    var serviceType: String?,
    var productTypes: List<String>?,
    var readyToEat: Boolean,
    var price: Int,
    var prepDescription: String?
)
