package com.eventsocial.domain.response

import com.eventsocial.domain.requests.Adress
import java.math.BigDecimal
import java.util.*

data class FilterSpacesReponse(
    val title: String,
    val spaceType: String,
    val images: Array<String>?,
    val services: Array<String>?,
    val price: BigDecimal?,
    val spaceCode: UUID,
    val ranking: Float,
    val rankingAmount: Int?,
    val address: Adress,
    val maximumOccupancy: Int?
)
