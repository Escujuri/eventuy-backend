package com.eventsocial.domain.response

import com.eventsocial.domain.entities.Spaces
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonPropertyOrder

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder("code", "message")
class InmuebleResponseDTO {
    /*---- GETTER SETTER----*/
    @JsonProperty("code")
    var code: String? = null

    @JsonProperty("message")
    var message: String? = null

    @JsonProperty("inmuebles")
    var inmuebles: List<Spaces>? = null

    @JsonProperty("inmueble")
    var inmueble: Spaces? = null
}
