package com.eventsocial.domain.response

data class OTPResponse(
    var code: String,
    var message: String
)
