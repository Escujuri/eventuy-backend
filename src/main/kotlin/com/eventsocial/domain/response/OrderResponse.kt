package com.eventsocial.domain.response

import com.eventsocial.domain.entities.Users
import com.eventsocial.services.TransactionalService
import java.time.Instant
import java.util.*
import javax.persistence.*

data class OrderResponse(
    var totalPrice: Double,
    var currency: String,
    var orderCode: UUID,
    var user: Users,
    var ownerUser: Users,
    var ownerApproval: String,
    var urlRedirect: String,
    var createdAt: Instant,
    var updatedAt: Instant?,
    var orderDateAt: Date,
    var street: String,
    var number: Int,
    var location: String,
    var city: String,
    var province: String,
    var country: String,
    var cp: String,
    val paymentStatus: TransactionalService.StatusPayment
)

data class OrderSummaryResponse(
    var totalPrice: Double,
    var currency: String,
    var orderCode: UUID,
    var userCode: UUID,
    var ownerUserCode: UUID,
    var ownerApproval: String,
    var companyCode: UUID,
    var urlRedirect: String,
    var createdAt: Instant,
    var updatedAt: Instant?,
    var orderDateAt: Date,
    var street: String,
    var number: Int,
    var location: String,
    var city: String,
    var province: String,
    var country: String,
    var cp: String,
    val paymentStatus: TransactionalService.StatusPayment,
    val products: List<ProductSumaryDTO>
)

data class ProductSumaryDTO(
    var id: Long?,
    var code: UUID?,
    var title: String?,
    var description: String?,
    var images: List<String>?,
    var serviceType: String?,
    var productTypes: List<String>?,
    var readyToEat: Boolean,
    var price: Int,
    var prepDescription: String?
)
