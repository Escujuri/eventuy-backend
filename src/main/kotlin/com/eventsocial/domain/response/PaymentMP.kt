package com.eventsocial.domain.response

data class PaymentMP(
    val code: String,
    val urlDirPago: String,
    val message: String,
    val paymentStatus: String,
)
