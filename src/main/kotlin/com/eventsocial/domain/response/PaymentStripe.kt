package com.eventsocial.domain.response

data class PaymentStripe(
    val code: String,
    val urlDirPago: String,
    val message: String,
    val paymentStatus: String,
)
