package com.eventsocial.domain.response

import com.fasterxml.jackson.annotation.JsonProperty

object RegisterDTO {
    /*---- GETTER SETTER----*/
    @JsonProperty("username")
    var username: String? = null

    @JsonProperty("password")
    var password: String? = null

    @JsonProperty("email")
    var email: String? = null
}
