package com.eventsocial.domain.response

data class RegisterResponse(
    var message: String,
    val userCaseId: String?,
    val sessionEnc: String?,
)
