package com.eventsocial.domain.response

import java.util.*

data class ReviewCompanyReponse(
    var userCode: UUID,
    var companyCode: UUID,
    var rating: Int,
    var comment: String,
    var description: String?,
    var image: String?,
    var userName: String?,
)
