package com.eventsocial.domain.response

import java.util.*

data class ReviewSpaceReponse(
    var userCode: UUID,
    var serviceCode: UUID,
    var rating: Int,
    var comment: String,
    var description: String?,
    var image: String?,
    var userName: String?,
)
