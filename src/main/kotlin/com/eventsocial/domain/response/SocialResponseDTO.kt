package com.eventsocial.domain.response

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonPropertyOrder

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder("code", "message")
class SocialResponseDTO {
    @JsonProperty("code")
    var code: String? = null

    @JsonProperty("message")
    var message: String? = null

    @JsonProperty("userCase")
    var userCaseId: String? = null

    @JsonProperty("sessionID")
    var sessionEnc: String? = null
}
