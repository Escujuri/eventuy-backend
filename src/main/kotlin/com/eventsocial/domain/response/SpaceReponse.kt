package com.eventsocial.domain.response

import com.eventsocial.domain.requests.Adress
import com.eventsocial.domain.requests.OpenHoursModel
import java.math.BigDecimal
import java.util.*

data class SpaceReponse(
    val userCode: UUID,
    val spaceCode: UUID,
    val title: String,
    val spaceType: String,
    val adress: Adress,
    val description: String?,
    val price: BigDecimal,
    val services: Array<String>?,
    val securityService: Array<String>?,
    val maximumOccupancy: Int?,
    val hoursAvailability: Array<String>?,
    val priceType: Boolean?,
    val publish: Boolean?,
    val datesNotAvailable: Array<String>?,
    val nameContact: String?,
    val lastNameContact: String?,
    val telephoneContact: String?,
    val cancellationsType: String?,
    val openHours: OpenHoursModel?,
    val images: Array<String>?,
)
