package com.eventsocial.domain.response

import com.eventsocial.domain.requests.Adress
import com.eventsocial.domain.requests.OpenHoursModel

data class SpaceSummary(
    val title: String?,
    val tipoLugar: String?,
    val adress: Adress?,
    val descripcion: String?,
    val price: Float?,
    val maxPersonas: Int?,
    val services: List<String>?,
    val serviciosDeSeguridad: List<String>?,
    val fotos: List<String>?,
    val esPrecioFijo: Boolean?,
    val disponibilidadPersonas: Int?,
    val openHours: OpenHoursModel,
    val puntuacion: Float?,
    val ranking: Float? // ranking y cant de reviews
)
