package com.eventsocial.domain.response

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonPropertyOrder

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder("code", "message")
data class UserInfoResponseDTO(
    @JsonProperty("code")
    var code: String? = null,
    @JsonProperty("message")
    var message: String? = null,
    //  @JsonProperty("usuario")
    //   var usuario: UsuarioDto? = null,
    @JsonProperty("favoritos")
    var favoritos: List<String>? = null,
    //   @JsonProperty("listaUsuario")
    //   var listUsuario: List<UsuarioDto>? = null,
)
