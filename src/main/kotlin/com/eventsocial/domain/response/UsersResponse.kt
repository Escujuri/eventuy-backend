package com.eventsocial.domain.response

import java.util.*
import kotlin.collections.List

data class UsersResponse(
    val name: String?,
    val lastName: String?,
    val age: Int?,
    val phone: String?,
    val code: UUID,
    val icon: String?,
    val birthDate: Date?,
    val documento: String?,
    val owner: Boolean,
    val email: String,
    val favorites: Array<String>?
)

data class UsersSummaryResponse(
    val name: String?,
    val lastName: String?,
    val age: Int?,
    val phone: String?,
    val code: UUID,
    val icon: String?,
    val birthDate: Date?,
    val documento: String?
)

data class UsersChatResponse(
    val userContacts: List<UsersSummaryResponse>
)
