package com.eventsocial.exceptions

class CustomException(
    message: String
) : Exception(message)
