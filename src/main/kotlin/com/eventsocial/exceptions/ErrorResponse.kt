package com.eventsocial.exceptions

data class ErrorResponse constructor(val code: String, val messages: List<String>)
