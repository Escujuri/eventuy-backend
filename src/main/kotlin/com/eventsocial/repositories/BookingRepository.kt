package com.eventsocial.repositories

import com.eventsocial.domain.entities.Booking
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import java.util.*

interface BookingRepository : JpaRepository<Booking, Long> {
    fun findByUserId_CodeOrderByCreatedAtDesc(userCode: UUID, of: PageRequest): Page<Booking>?

    fun findByUserId_CodeAndOwnerApprovalOrderByCreatedAtDesc(userCode: UUID, status: String, of: PageRequest): Page<Booking>?

    @Query("SELECT * FROM Booking where code=:bookingCode and (users_id=:userId or owner_user_id=:userId)", nativeQuery = true)
    fun findByCodeValidated(userId: Int, bookingCode: UUID): Booking?

    @Query("SELECT b.* FROM Booking b INNER JOIN Transactional t on b.id =t.booking_id where t.payment_status='unpaid' and b.users_id=:userId", nativeQuery = true)
    fun findBookingPaidByUserId(userId: Int): List<Booking>

    @Query("SELECT b.* FROM Booking b INNER JOIN Transactional t on b.id =t.booking_id where t.payment_status='unpaid' and b.owner_user_id=:userId", nativeQuery = true)
    fun findBookingPaidByUserIdOwner(userId: Int): List<Booking>

    fun findByOwnerUserId_CodeAndOwnerApprovalOrderByCreatedAtDesc(userCode: UUID, status: String, of: PageRequest): Page<Booking>?
    fun findByOwnerUserId_CodeOrderByCreatedAtDesc(userCode: UUID, of: PageRequest): Page<Booking>?

    fun findByCodeAndOwnerUserId_Code(bookingCode: UUID, userCode: UUID): Booking?

    fun findByCodeAndUserId_Code(bookingCode: UUID, userCode: UUID): Booking?

    fun findByCode(bookingCode: UUID): Booking?
}
