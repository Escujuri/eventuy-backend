package com.eventsocial.repositories

import com.eventsocial.domain.entities.Company
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface CompanyRepository : JpaRepository<Company, Long>, JpaSpecificationExecutor<Company> {
    fun findByServiceCode(serviceCode: UUID): Optional<Company?>
    fun findByServiceCodeAndUserId_Code(serviceCode: UUID, userCode: UUID): Optional<Company?>
    fun findByUserId_Code(userCode: UUID, of: PageRequest): Page<Company?>
}
