package com.eventsocial.repositories

import com.eventsocial.domain.entities.Order
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface OrderRepository : JpaRepository<Order, Long> {
    fun findByUserCodeOrderByCreatedAt(userCode: UUID): List<Order?>

    fun findByCode(oderCode: UUID): Order?

    fun findByOwnerUser_CodeAndCode(userCode: UUID, orderCode: UUID): Order?
    fun findByCodeAndUser_Code(orderCode: UUID, userCode: UUID): Order?
    fun findByUser_CodeOrderByCreatedAt(userCode: UUID, of: PageRequest): Page<Order>?
    fun findByUser_CodeAndOwnerApprovalOrderByCreatedAt(userCode: UUID, status: String, of: PageRequest): Page<Order>?

    fun findByOwnerUser_CodeAndOwnerApprovalOrderByCreatedAt(userCode: UUID, status: String, of: PageRequest): Page<Order>?

    fun findByOwnerUser_CodeOrderByCreatedAt(userCode: UUID, of: PageRequest): Page<Order>?
}
