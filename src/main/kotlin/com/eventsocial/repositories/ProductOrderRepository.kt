package com.eventsocial.repositories

import com.eventsocial.domain.entities.ProductOrder
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface ProductOrderRepository : JpaRepository<ProductOrder, Long> {

    fun findByOrder_UserCode(userCode: UUID): List<ProductOrder?>

    fun findByOrderId(orderId: Long): List<ProductOrder>
    fun findByOrder_UserCode(userCode: UUID, of: PageRequest): Page<ProductOrder>?
}
