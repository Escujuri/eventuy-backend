package com.eventsocial.repositories

import com.eventsocial.domain.entities.Product
import org.springframework.data.jpa.repository.JpaRepository

interface ProductRepository : JpaRepository<Product, Long>
