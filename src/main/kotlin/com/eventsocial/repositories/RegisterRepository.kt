package com.eventsocial.repositories

import com.eventsocial.domain.entities.Register
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface RegisterRepository : JpaRepository<Register, Long> {
    fun findByEmailAndValidated(email: String, validated: Boolean = true): Register?

    fun findByEmail(email: String): Register?
}
