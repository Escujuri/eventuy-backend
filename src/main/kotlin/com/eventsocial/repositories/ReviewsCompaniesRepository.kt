package com.eventsocial.repositories

import com.eventsocial.domain.entities.ReviewsCompanies
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface ReviewsCompaniesRepository : JpaRepository<ReviewsCompanies, Long> {

    fun findByCompanyId_ServiceCode(serviceCode: UUID): List<ReviewsCompanies?>

    fun findByUserCode(userCode: UUID): List<ReviewsCompanies?>
}
