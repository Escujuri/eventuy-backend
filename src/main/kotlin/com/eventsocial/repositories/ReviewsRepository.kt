package com.eventsocial.repositories

import com.eventsocial.domain.entities.Reviews
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface ReviewsRepository : JpaRepository<Reviews, Long> {

    fun findBySpaceId_Code(spaceCode: UUID): List<Reviews?>

    fun findByUserCode(restaurantCode: UUID): List<Reviews?>
}
