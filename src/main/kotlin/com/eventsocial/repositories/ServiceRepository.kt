package com.eventsocial.repositories

import com.eventsocial.domain.entities.Services
import org.springframework.data.jpa.repository.JpaRepository

interface ServiceRepository : JpaRepository<Services, Long>
