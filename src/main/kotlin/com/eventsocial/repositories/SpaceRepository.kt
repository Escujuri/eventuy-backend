package com.eventsocial.repositories

import com.eventsocial.domain.entities.Spaces
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import java.util.*

interface SpaceRepository : JpaRepository<Spaces, Long>, JpaSpecificationExecutor<Spaces> {

    fun findByUserCode(userCode: UUID, of: PageRequest): Page<Spaces>?

    fun findByUserCode(userCode: UUID): List<Spaces>?

    fun findByCode(spaceCode: UUID): Spaces?

    fun deleteByCode(spaceCode: UUID)

    fun findBySpaceTypeAndMaximumOccupancy(spaceType: String, maximumOccupancy: Int, of: PageRequest): Page<Spaces>

    fun findBySpaceType(spaceType: String, of: PageRequest): Page<Spaces>

    fun findByMaximumOccupancy(maximumOccupancy: Int, of: PageRequest): Page<Spaces>
}
