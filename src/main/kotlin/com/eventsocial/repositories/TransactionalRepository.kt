package com.eventsocial.repositories

import com.eventsocial.domain.entities.Transactional
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface TransactionalRepository : JpaRepository<Transactional, Long> {

    fun findByBookingId_Id(booking: Long): Transactional?

    fun findByOrderId_Id(orderId: Long): Transactional?

    fun findByBookingId_Code(bookingCode: UUID): Transactional?

    fun findByOrderId_Code(orderCode: UUID): Transactional?

    fun findByCollectorId(collectorId: String): Transactional

    fun findByBookingId_CodeAndBookingId_UserId_Code(bookingCode: UUID, userCode: UUID): Transactional?

    fun findByBookingId_CodeAndBookingId_OwnerUserId_Code(bookingCode: UUID, userCode: UUID): Transactional?

    fun findByIdempotencyId(idempotencyId: String): Transactional
}
