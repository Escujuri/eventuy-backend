package com.eventsocial.repositories

import com.eventsocial.domain.entities.Users
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import java.util.*

interface UserRepository : JpaRepository<Users, Long> {

    @Query("SELECT * FROM Users a WHERE a.register_id = :registerId", nativeQuery = true)
    fun getUserByRegisterId(registerId: Long): Users

    fun findByCode(userCode: UUID): Users
}
