package com.eventsocial.repositories.sql

object RegisterSql {

    const val LOGIN_USER = """
    select * from register where email = :email
    """
    const val LOGIN_DUPLICATE_USER = """
     select * from register where email = :email
    """

    const val EMAIL_BY_USERID = """
   select email from register" where id = :userId
   """

    const val RESET_PASSWORD_BY_USERID = """
        update register set pass = :password where id = :userId        
    """

    const val POST_DATA_OTP_VALIDATION = """
        update Usuario_Login set otpCode = :otp , dateValidationOTP= :dateOtp where id = : userId
    """
}
