package com.eventsocial.repositories.sql

object TransactionalSql {

    const val FIND_BY_FILTER = """
       select * from spaces r where r.space_type = :spaceType
       or r.maximum_occupancy = :maximumOccupancy
    """

    const val UPDATE_PAYMENT = """
     Update Transacciones set statusPago = ?, formaPago = ?, currency_id = ?
     , fechaAprobado = ? , montoNetoRecibido = ? , payment_id = ? 
     , paymentType = ? , paymentTypeId = ? Where idempotencia_id = ? 
    """

    const val SEACH_BOOKING_BY_ID_USERID = """
       select * from Transacciones where userId = ?", 
    """

    const val SEACH_BOOKING_BY_ID_OWNER_USER_ID = """
       select * from Transacciones where ownerUserId = ?"
    """

    const val SEACH_BOOKING_CHAT_BY_USER_ID_ORGANIZER = """
     select * from Transacciones where userId = ? and ownerApproval='ACCEPTED' 
    """

    const val SEACH_BOOKING_CHAT_BY_USER_ID_OWNER = """
        select * from Transacciones where ownerUserId = ? and ownerApproval='ACCEPTED'
    """

    const val SEACH_BOOKING_BY_ID_INMUEBLEID = """
      select *  from Transacciones" where idInmueble = ? and fechaReserva >= CURDATE()  
    """

    const val SEACH_BOOKING_BY_IDEMPOTENCY = """
       select * from Transaccione where idempotencia_id = ?
    """

    const val UPDATE_APPROVAL = """
        Update Transacciones set ownerApproval = ? Where payment_id = ? 
    """
}
