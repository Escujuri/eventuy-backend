package com.eventsocial.repositories.sql

object UsersSql {
    const val GET_USER_INFO = "getUserInfo"
    const val GET_LIST_USER_INFO = "getListUserInfo"
}
