package com.eventsocial.security.jwt

interface ITokenExtractor {

    fun Extract(payload: String): String

    fun ReadToken(token: String?): Map<String?, Any?>?
}
