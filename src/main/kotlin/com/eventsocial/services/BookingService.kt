package com.eventsocial.services

import com.eventsocial.clients.SengridApiClient
import com.eventsocial.domain.entities.Booking
import com.eventsocial.domain.mappers.BookignMapper.toBookingReponse
import com.eventsocial.domain.requests.CreateBooking
import com.eventsocial.domain.response.BookingResponse
import com.eventsocial.repositories.BookingRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.server.ResponseStatusException
import java.time.Instant
import java.util.*

@Service
class BookingService {

    @Autowired
    private lateinit var spaceService: SpaceService

    @Autowired
    private lateinit var transactionalService: TransactionalService

    @Autowired
    private lateinit var sengridApiClient: SengridApiClient

    @Autowired
    private lateinit var bookingRepository: BookingRepository

    @Autowired
    private lateinit var userService: UserService

    enum class StatusOwnerApproval {
        CREATED, REJECTED, ACCEPTED
    }

    @Transactional(rollbackFor = [Exception::class])
    fun createBooking(createBooking: CreateBooking, userCode: UUID) {
        spaceService.getSpaceByCode(createBooking.spaceCode)?.let { space ->
            if (space.userCode == userCode)
                throw ResponseStatusException(HttpStatus.CONFLICT, "User code:$userCode, can not book yourself")

            val organizerInfo = userService.getUser(userCode)
            val ownerInfo = userService.getUser(space.userCode)

            bookingRepository.save(
                Booking(
                    userId = organizerInfo,
                    spaceId = space,
                    ownerApproval = StatusOwnerApproval.CREATED.name,
                    occupancy = createBooking.occupancy,
                    ownerUserId = ownerInfo,
                    dateCheckIn = createBooking.date.dateCheckIn,
                    dateCheckOut = createBooking.date.dateCheckOut,
                    createdAt = Instant.now(),
                    amount = createBooking.amount,
                    currency = createBooking.currency,
                    urlRedirect = createBooking.urlRedirect,
                    updatedAt = null
                )
            ).let {
                sengridApiClient.sendOwnerBooking(space, createBooking, organizerInfo, ownerInfo)
            }
        } ?: throw ResponseStatusException(HttpStatus.CONFLICT, "Space Code: ${createBooking.spaceCode}, not found")
    }

    @Transactional
    fun getBookingByUserCode(userCode: UUID, owner: Boolean, status: StatusOwnerApproval?, offset: Int, page: Int) =
        if (!owner && status?.name.isNullOrEmpty()) {
            bookingRepository.findByUserId_CodeOrderByCreatedAtDesc(userCode, PageRequest.of(page, offset))?.map {
                it.toBookingReponse(transactionalService.getTransactionByBookingId(it.id))
            }
        } else if (!owner && !status?.name.isNullOrEmpty()) {
            bookingRepository.findByUserId_CodeAndOwnerApprovalOrderByCreatedAtDesc(userCode, status!!.name, PageRequest.of(page, offset))?.map {
                it.toBookingReponse(transactionalService.getTransactionByBookingId(it.id))
            }
        } else if (owner && !status?.name.isNullOrEmpty()) {
            bookingRepository.findByOwnerUserId_CodeAndOwnerApprovalOrderByCreatedAtDesc(userCode, status!!.name, PageRequest.of(page, offset))?.map {
                it.toBookingReponse(transactionalService.getTransactionByBookingId(it.id))
            }
        } else {
            bookingRepository.findByOwnerUserId_CodeOrderByCreatedAtDesc(userCode, PageRequest.of(page, offset))?.map {
                it.toBookingReponse(transactionalService.getTransactionByBookingId(it.id))
            }
        }

    @Transactional(rollbackFor = [Exception::class])
    fun getBookingByCode(userId: Int, bookingCode: UUID) =
        bookingRepository.findByCodeValidated(userId, bookingCode)?.let {
            it.toBookingReponse(transactionalService.getTransactionByBookingId(it.id))
        } ?: throw ResponseStatusException(HttpStatus.CONFLICT, "Booking code: $bookingCode not found")

    @Transactional(rollbackFor = [Exception::class])
    fun getBookingByCodeAndUserCode(bookingCode: UUID, userCode: UUID) = bookingRepository.findByCodeAndUserId_Code(bookingCode, userCode)

    @Transactional(rollbackFor = [Exception::class])
    fun pathBookingByBookingCode(
        userCode: UUID,
        bookingCode: UUID,
        statusOwnerApproval: StatusOwnerApproval,
        urlRedirect: String
    ): BookingResponse =
        bookingRepository.findByCodeAndOwnerUserId_Code(bookingCode, userCode)?.let {
            if (it.ownerApproval.equals(StatusOwnerApproval.ACCEPTED) || statusOwnerApproval.name.equals(StatusOwnerApproval.REJECTED))
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Booking Code: $bookingCode, you cannot reject a reservation, after accepting it")

            it.ownerApproval = statusOwnerApproval.name
            bookingRepository.save(it).let { booking ->
                sengridApiClient.bookingStatusOrganizer(it, urlRedirect)
                return booking.toBookingReponse(transactionalService.getTransactionByBookingId(it.id))
            }
        } ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Booking Code: $bookingCode not found or you arent the owner")
}
