package com.eventsocial.services

import com.eventsocial.domain.entities.Company
import com.eventsocial.domain.mappers.CompanyMapper.toCompanyReponse
import com.eventsocial.domain.mappers.CompanySumaryMapper.toCompanySumaryReponse
import com.eventsocial.domain.requests.CompanySummaryDTO
import com.eventsocial.domain.requests.CreateCompanyDTO
import com.eventsocial.domain.requests.UpdateCompanyDTO
import com.eventsocial.domain.response.CompanyReponse
import com.eventsocial.domain.response.CompanySumaryReponse
import com.eventsocial.exceptions.CustomException
import com.eventsocial.repositories.CompanyRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Service
class CompanyService {

    @Autowired
    lateinit var companyRepository: CompanyRepository

    @Autowired
    lateinit var userService: UserService

    fun createCompany(userCode: UUID, company: CreateCompanyDTO): Company {
        val newCompany = Company(
            serviceCategoryObject = company.serviceCategoryObject,
            title = company.title,
            description = company.description,
            image = company.image,
            address = company.address,
            withShipping = company.withShipping,
            distanceXprice = company.distanceXprice,
            antelationNumber = company.antelationNumber,
            antelationTime = company.antelationTime,
            shipmentDays = company.shipmentDays,
            shipmentHourStart = company.shipmentHourStart,
            shipmentHourEnd = company.shipmentHourEnd,
            freeShipping = company.freeShipping,
            freeShippingAfterPrice = company.freeShippingAfterPrice,
            freeShippingMaxDistance = company.freeShippingMaxDistance,
            productList = company.productList,
            userId = userService.getUser(userCode),
            cancellationsType = company.cancellationsType,
            ranking = company.ranking,
            rankingAmount = company.rankingAmount,
        )

        return companyRepository.save(newCompany)
    }

    fun getCompanyByCode(serviceCode: UUID): Company? {
        return companyRepository.findByServiceCode(serviceCode).orElse(null)
    }

    fun getCompanyByUserCode(userCode: UUID, offset: Int, page: Int): Page<CompanySumaryReponse?> {
        return companyRepository.findByUserId_Code(userCode, PageRequest.of(page, offset)).map { it?.toCompanySumaryReponse() }
    }

    fun updateCompany(serviceCode: UUID, updatedCompany: UpdateCompanyDTO, userCode: UUID?): CompanyReponse? {
        val company = companyRepository.findByServiceCodeAndUserId_Code(serviceCode, userCode!!)
        if (company.isPresent) {
            val existingCompany = company.get()
            existingCompany.serviceCategoryObject = updatedCompany.serviceCategoryObject ?: existingCompany.serviceCategoryObject
            existingCompany.title = updatedCompany.title ?: existingCompany.title
            existingCompany.description = updatedCompany.description ?: existingCompany.description
            existingCompany.image = updatedCompany.image ?: existingCompany.image
            existingCompany.address = updatedCompany.address ?: existingCompany.address
            existingCompany.withShipping = updatedCompany.withShipping ?: existingCompany.withShipping
            existingCompany.distanceXprice = updatedCompany.distanceXprice ?: existingCompany.distanceXprice
            existingCompany.antelationNumber = updatedCompany.antelationNumber ?: existingCompany.antelationNumber
            existingCompany.antelationTime = updatedCompany.antelationTime ?: existingCompany.antelationTime
            existingCompany.shipmentDays = updatedCompany.shipmentDays ?: existingCompany.shipmentDays
            existingCompany.shipmentHourStart = updatedCompany.shipmentHourStart ?: existingCompany.shipmentHourStart
            existingCompany.shipmentHourEnd = updatedCompany.shipmentHourEnd ?: existingCompany.shipmentHourEnd
            existingCompany.freeShipping = updatedCompany.freeShipping ?: existingCompany.freeShipping
            existingCompany.freeShippingAfterPrice = updatedCompany.freeShippingAfterPrice ?: existingCompany.freeShippingAfterPrice
            existingCompany.freeShippingMaxDistance = updatedCompany.freeShippingMaxDistance ?: existingCompany.freeShippingMaxDistance
            existingCompany.productList = updatedCompany.productList ?: existingCompany.productList
            existingCompany.cancellationsType = updatedCompany.cancellationsType ?: existingCompany.cancellationsType

            return companyRepository.save(existingCompany).toCompanyReponse()
        }
        return null
    }

    fun deleteCompany(serviceCode: UUID, userCode: UUID?) {
        val company = companyRepository.findByServiceCodeAndUserId_Code(serviceCode, userCode!!)
        if (company.isPresent) {
            companyRepository.delete(company.get())
        }
    }

    fun getAllCompanies(
        serviceCategory: String?,
        withShipping: Boolean?,
        minPrice: Double?,
        maxPrice: Double?,
        searchQuery: String?,
        offset: Int,
        page: Int
    ): Page<CompanySummaryDTO> {
        var companies = companyRepository.findAll(PageRequest.of(page, offset)).content

        serviceCategory?.let {
            companies = companies.filter { company -> it in company.serviceCategoryObject }
        }

        withShipping?.let {
            companies = companies.filter { company -> company.withShipping == it }
        }

        minPrice?.let {
            companies = companies.filter { company -> company.productList.any { product -> product.price >= it } }
        }

        maxPrice?.let {
            companies = companies.filter { company -> company.productList.any { product -> product.price <= it } }
        }

        searchQuery?.let {
            companies = searchCompanies(companies, it)
        }

        val pagedList = companies
            .map { company ->
                CompanySummaryDTO(
                    serviceCategoryObject = company.serviceCategoryObject,
                    title = company.title,
                    description = company.description,
                    withShipping = company.withShipping,
                    serviceCode = company.serviceCode,
                    freeShipping = company.freeShipping,
                    address = company.address,
                    image = company.image,
                    ranking = company.ranking,
                    rankingAmount = company.rankingAmount
                )
            }
            .let { PageImpl(it, PageRequest.of(page, offset), it.size.toLong()) }

        return pagedList
    }

    private fun searchCompanies(companies: List<Company>, searchQuery: String): List<Company> {
        val sortedCompanies = companies.sortedBy { it.title }
        val lowerCasedQuery = searchQuery.lowercase()

        var left = 0
        var right = sortedCompanies.size - 1
        val results = mutableListOf<Company>()

        while (left <= right) {
            val mid = (left + right) / 2
            val company = sortedCompanies[mid]
            val title = company.title.lowercase()

            if (title.contains(lowerCasedQuery)) {
                results.add(company)
            }

            if (title >= lowerCasedQuery) {
                right = mid - 1
            } else {
                left = mid + 1
            }
        }

        return results
    }

    @Transactional
    fun updateRatingCompany(serviceCode: UUID, ranking: Float, counter: Int) {
        companyRepository.findByServiceCode(serviceCode).let { it ->
            if (it.isPresent) {
                it.get().let { it ->
                    it.id = it.id
                    it.ranking = ranking
                    it.rankingAmount = counter
                    it.userId = it.userId
                    companyRepository.save(it)
                }
            } else {
                throw CustomException("Company code:$serviceCode, not found")
            }
        }
    }
}
