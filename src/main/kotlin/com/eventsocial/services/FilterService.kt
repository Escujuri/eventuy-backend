package com.eventsocial.services

import com.eventsocial.domain.entities.Address
import com.eventsocial.domain.entities.Company
import com.eventsocial.domain.entities.Spaces
import com.eventsocial.domain.requests.SpacesTypes
import com.eventsocial.repositories.CompanyRepository
import com.eventsocial.repositories.SpaceRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.math.BigDecimal
import javax.persistence.criteria.Join
import javax.persistence.criteria.Predicate

@Service
class FilterService {

    @Autowired
    private lateinit var spaceRepository: SpaceRepository

    @Autowired
    private lateinit var companyRepository: CompanyRepository

    @Transactional
    fun getFilterSpaces(
        spaceType: List<SpacesTypes>?,
        maximumOccupancy: Int?,
        priceMin: BigDecimal?,
        priceMax: BigDecimal?,
        country: List<String>?,
        provinces: List<String>?,
        ranking: Int?,
        services: List<String>?, // Agregamos el filtro de servicios
        page: Int,
        offset: Int,
        sortBy: String,
        sortOrder: String
    ): Page<Spaces> {
        val pageable = PageRequest.of(page, offset, Sort.by(Sort.Direction.fromString(sortOrder), sortBy))

        // Construir una especificación de búsqueda dinámica en función de los parámetros de filtro.
        val specification = Specification<Spaces> { root, _, criteriaBuilder ->
            var predicate: Predicate? = null

            // Filtrar por espacio tipo (si se proporciona).
            spaceType?.takeIf { it.isNotEmpty() }?.let {
                val spaceTypePredicate = root.get<String>("spaceType").`in`(spaceType.map { it.toString() })
                predicate = criteriaBuilder.and(spaceTypePredicate)
            }

            // Filtrar por ocupación máxima (si se proporciona).
            maximumOccupancy?.let {
                val maximumOccupancyPredicate = criteriaBuilder.greaterThanOrEqualTo(root.get("maximumOccupancy"), maximumOccupancy)
                predicate = predicate?.let { criteriaBuilder.and(it, maximumOccupancyPredicate) } ?: maximumOccupancyPredicate
            }

            priceMin?.let {
                val priceMinPredicate = criteriaBuilder.greaterThanOrEqualTo(root.get("price"), priceMin)
                predicate = predicate?.let { criteriaBuilder.and(it, priceMinPredicate) } ?: priceMinPredicate
            }

            // Filtrar por precio máximo (si se proporciona).
            priceMax?.let {
                val priceMaxPredicate = criteriaBuilder.lessThanOrEqualTo(root.get("price"), priceMax)
                predicate = predicate?.let { criteriaBuilder.and(it, priceMaxPredicate) } ?: priceMaxPredicate
            }

            // Filtrar por países (si se proporcionan).
            country?.takeIf { it.isNotEmpty() }?.let { countryList ->
                val countryPredicates = countryList.map { country ->
                    // Utilizar criteriaBuilder.like para realizar una búsqueda insensible a mayúsculas y minúsculas.
                    criteriaBuilder.like(criteriaBuilder.lower(root.get<String>("country")), country.toLowerCase())
                }
                val countryPredicate = criteriaBuilder.or(*countryPredicates.toTypedArray())
                predicate = predicate?.let { criteriaBuilder.and(it, countryPredicate) } ?: countryPredicate
            }

            // Filtrar por provincias (si se proporcionan).
            provinces?.takeIf { it.isNotEmpty() }?.let { provinceList ->
                val provincePredicates = provinceList.map { province ->
                    // Utilizar criteriaBuilder.like para realizar una búsqueda insensible a mayúsculas y minúsculas.
                    criteriaBuilder.like(criteriaBuilder.lower(root.get<String>("province")), province.toLowerCase())
                }
                val provincePredicate = criteriaBuilder.or(*provincePredicates.toTypedArray())
                predicate = predicate?.let { criteriaBuilder.and(it, provincePredicate) } ?: provincePredicate
            }

            // Filtrar por ranking (si se proporciona).
            ranking?.let {
                val rankingPredicate = criteriaBuilder.equal(root.get<Int>("ranking"), it)
                predicate = predicate?.let { criteriaBuilder.and(it, rankingPredicate) } ?: rankingPredicate
            }

            predicate ?: criteriaBuilder.conjunction()
        }

        // Utilizar el repositorio para realizar la consulta con la especificación y paginación.
        return spaceRepository.findAll(specification, pageable)
    }

    @Transactional
    fun getCompaniesByMultipleFilters(
        categories: List<String>?,
        provinces: List<String>?,
        freeShipping: Boolean?,
        offset: Int,
        page: Int,
        sortBy: String?,
        sortOrder: String?
    ): Page<Company> {
        val pageable = if (sortOrder.equals("asc", ignoreCase = true)) {
            PageRequest.of(page, offset, Sort.by(Sort.Direction.ASC, sortBy))
        } else {
            PageRequest.of(page, offset, Sort.by(Sort.Direction.DESC, sortBy))
        }
        // Construir especificación de consulta
        val spec = CompanySpecifications.withFilters(
            categories, provinces, freeShipping,
        )
        // Ejecutar consulta paginada utilizando el nuevo método
        return companyRepository.findAll(spec, pageable)
    }
}

object CompanySpecifications {
    fun withFilters(
        categories: List<String>?,
        provinces: List<String>?,
        freeShipping: Boolean?,
    ): Specification<Company> {
        return Specification { root, _, criteriaBuilder ->
            val predicates = mutableListOf<Predicate>()

            // Ejemplo de cómo agregar un predicado para categories:
            categories?.let { cats ->
                if (cats.isNotEmpty()) {
                    // Inicializamos una lista de predicados para cada categoría
                    val categoryPredicates = cats.map { category ->
                        val joinCategory: Join<Company, String> = root.join("serviceCategoryObject")
                        // Utilizar criteriaBuilder.like para realizar una búsqueda insensible a mayúsculas y minúsculas.
                        return@map criteriaBuilder.like(criteriaBuilder.lower(joinCategory), category.toLowerCase())
                    }
                    // Unimos todos los predicados con un AND
                    val allCategoriesPredicate = criteriaBuilder.and(*categoryPredicates.toTypedArray())
                    predicates.add(allCategoriesPredicate)
                }
            }

            // Ejemplo de cómo agregar un predicado para provinces:
            provinces?.let { provs ->
                if (provs.isNotEmpty()) {
                    val joinAddress: Join<Company, Address> = root.join("address")
                    val provincePredicates = provs.map { province ->
                        // Utilizar criteriaBuilder.like para realizar una búsqueda insensible a mayúsculas y minúsculas.
                        return@map criteriaBuilder.like(criteriaBuilder.lower(joinAddress.get<String>("province")), province.toLowerCase())
                    }
                    val allProvincesPredicate = criteriaBuilder.and(*provincePredicates.toTypedArray())
                    predicates.add(allProvincesPredicate)
                }
            }

            // Ejemplo de cómo agregar un predicado para freeShipping:
            if (freeShipping != null) {
                val freeShippingPredicate = criteriaBuilder.equal(root.get<Boolean>("freeShipping"), freeShipping)
                predicates.add(freeShippingPredicate)
            }

            // Combine los predicados con un AND
            criteriaBuilder.and(*predicates.toTypedArray())
        }
    }
}
