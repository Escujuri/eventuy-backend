package com.eventsocial.services

import com.eventsocial.config.ConfigCommon
import com.eventsocial.domain.requests.ChangePassword
import com.eventsocial.domain.response.RegisterResponse
import com.eventsocial.repositories.RegisterRepository
import com.eventsocial.repositories.SpaceRepository
import com.eventsocial.repositories.UserRepository
import com.eventsocial.security.jwt.JwtTokenUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import java.util.*
import javax.transaction.Transactional
import kotlin.collections.HashMap

@Service
class LoginService {

    @Autowired
    private lateinit var registerRepository: RegisterRepository

    @Autowired
    private lateinit var userRepository: UserRepository

    @Autowired
    private lateinit var spaceRepository: SpaceRepository

    @Autowired
    private lateinit var configCommon: ConfigCommon

    @Autowired
    private lateinit var jwtTokenUtil: JwtTokenUtil

    @Autowired
    private lateinit var jwtInMemoryUserDetailsService: UserDetailsService

    @Transactional
    fun authenticate(email: String, password: String): RegisterResponse {

        registerRepository.findByEmail(email)?.let {

            if (it.password != configCommon.hashWith256(password)) {
                throw ResponseStatusException(HttpStatus.CONFLICT, "Password invalid for email: $email")
            }

            /*
             * Etapa de completar el JWT
             */
            val claims: MutableMap<String, Any?> = HashMap()
            val userDetails = jwtInMemoryUserDetailsService.loadUserByUsername(email)
            val userInfo = userRepository.getUserByRegisterId(it.id)

            claims["email"] = it.email
            claims["userId"] = userInfo.id
            claims["userCode"] = userInfo.code
            claims["name"] = userInfo.name
            claims["icon"] = userInfo.icon
            claims["age"] = userInfo.age
            claims["birthDate"] = userInfo.birthDate
            claims["owner"] = !spaceRepository.findByUserCode(it.code).isNullOrEmpty()

            return RegisterResponse(
                message = "Se logeo correctamente",
                sessionEnc = jwtTokenUtil.generateToken(userDetails, claims),
                userCaseId = "usuario_loggeado_correctamente"
            )
        } ?: throw ResponseStatusException(HttpStatus.CONFLICT, "User email: $email not found")
    }

    @Transactional
    fun resetPassword(changePassword: ChangePassword): RegisterResponse {
        registerRepository.findByEmail(changePassword.email).let {
            if (configCommon.hashWith256(changePassword.oldPassword) == it!!.password) {
                registerRepository.save(
                    it.apply {
                        password =
                            configCommon.hashWith256(changePassword.newPassword)!!
                    }
                )
                return RegisterResponse(
                    message = "Contraseña actualizada",
                    sessionEnc = null,
                    userCaseId = null,
                )
            } else {
                throw ResponseStatusException(HttpStatus.CONFLICT, "the password for user email:${changePassword.email} is wrong")
            }
        }
        throw ResponseStatusException(HttpStatus.CONFLICT, "User email: ${changePassword.email} not found")
    }
}
