package com.eventsocial.services

import com.eventsocial.clients.SengridApiClient
import com.eventsocial.domain.entities.Order
import com.eventsocial.domain.entities.ProductOrder
import com.eventsocial.domain.mappers.OrderMapper.toOrderResponse
import com.eventsocial.domain.mappers.OrderMapper.toOrderSummaryResponse
import com.eventsocial.domain.requests.CreateOrder
import com.eventsocial.domain.response.OrderResponse
import com.eventsocial.domain.response.OrderSummaryResponse
import com.eventsocial.repositories.OrderRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.server.ResponseStatusException
import java.time.Instant
import java.util.*

@Service
class OrderService {

    @Autowired
    private lateinit var orderRepository: OrderRepository

    @Autowired
    private lateinit var productOrderService: ProductOrderService

    @Autowired
    private lateinit var productService: ProductService

    @Autowired
    private lateinit var sengridApiClient: SengridApiClient

    @Autowired
    private lateinit var userService: UserService

    @Autowired
    private lateinit var transactionalService: TransactionalService

    enum class StatusOwnerApproval {
        CREATED, REJECTED, ACCEPTED
    }
    @Transactional
    fun getOrderByUserCode(
        userCode: UUID,
        owner: Boolean,
        status: BookingService.StatusOwnerApproval?,
        offset: Int,
        page: Int
    ) =
        if (!owner && status?.name.isNullOrEmpty()) {
            orderRepository.findByUser_CodeOrderByCreatedAt(userCode, PageRequest.of(page, offset))?.map {
                val products = productOrderService.getProductsByOrderId(it.id)
                it.toOrderSummaryResponse(transactionalService.getTransactionByOrderId(it.id), products)
            }
        } else if (!owner && !status?.name.isNullOrEmpty()) {
            orderRepository.findByUser_CodeAndOwnerApprovalOrderByCreatedAt(
                userCode,
                status!!.name,
                PageRequest.of(page, offset)
            )?.map {
                val products = productOrderService.getProductsByOrderId(it.id)
                it.toOrderSummaryResponse(transactionalService.getTransactionByOrderId(it.id), products)
            }
        } else if (owner && !status?.name.isNullOrEmpty()) {
            orderRepository.findByOwnerUser_CodeAndOwnerApprovalOrderByCreatedAt(
                userCode,
                status!!.name,
                PageRequest.of(page, offset)
            )?.map {
                val products = productOrderService.getProductsByOrderId(it.id)
                it.toOrderSummaryResponse(transactionalService.getTransactionByOrderId(it.id), products)
            }
        } else {
            orderRepository.findByOwnerUser_CodeOrderByCreatedAt(userCode, PageRequest.of(page, offset))?.map {
                val products = productOrderService.getProductsByOrderId(it.id)
                it.toOrderSummaryResponse(transactionalService.getTransactionByOrderId(it.id), products)
            }
        }

    @Transactional(rollbackFor = [Exception::class])
    fun createOrder(userCode: UUID, createOrder: CreateOrder) {
        // Tomamos el primero dado que siempre va a venir con algun producto
        var ownerUser = productService.getProductById(createOrder.order.products[0].productId).company.userId
        var user = userService.getUser(userCode)
        orderRepository.save(
            Order(
                totalPrice = createOrder.totalPrice,
                createdAt = Instant.now(),
                currency = createOrder.currency,
                city = createOrder.address.city,
                country = createOrder.address.country,
                cp = createOrder.address.cp,
                location = createOrder.address.location,
                number = createOrder.address.number,
                province = createOrder.address.province,
                street = createOrder.address.street,
                user = user,
                ownerUser = ownerUser,
                urlRedirect = createOrder.urlRedirect,
                ownerApproval = StatusOwnerApproval.CREATED.name,
                orderDateAt = createOrder.orderDateAt
            )
        ).let {
            createOrder.order.products.map { product ->
                val unitProduct = productService.getProductById(product.productId)
                productOrderService.createProductOrder(
                    ProductOrder(
                        product = unitProduct,
                        totalPrice = product.totalPrice,
                        quantity = product.quantity,
                        order = it
                    )
                )
            }
            sengridApiClient.sendOwnerOrder(createOrder, user, ownerUser)
        }
    }

    @Transactional(rollbackFor = [Exception::class])
    fun pathOrderByOrderCode(
        userCode: UUID,
        orderCode: UUID,
        statusOwnerApproval: StatusOwnerApproval,
        urlRedirect: String
    ): OrderResponse =
        orderRepository.findByOwnerUser_CodeAndCode(userCode, orderCode)?.let {
            if (it.ownerApproval.equals(StatusOwnerApproval.ACCEPTED) || statusOwnerApproval.name.equals(
                    StatusOwnerApproval.REJECTED
                )
            )
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Order Code: $orderCode, you cannot reject a order, after accepting it")
            it.ownerApproval = statusOwnerApproval.name
            orderRepository.save(it).let { order ->
                sengridApiClient.orderStatusOrganizer(it, urlRedirect)
                return order.toOrderResponse(transactionalService.getTransactionByOrderId(it.id))
            }
        } ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Order Code: $orderCode not found or you arent the owner")

    @Transactional(rollbackFor = [Exception::class])
    fun getOrderByCodeAndUserCode(orderCode: UUID, userCode: UUID) = orderRepository.findByCodeAndUser_Code(orderCode, userCode)

    @Transactional(rollbackFor = [Exception::class])
    fun getOrderByCode(orderCode: UUID): OrderSummaryResponse? {
        orderRepository.findByCode(orderCode)?.let {
            val products = productOrderService.getProductsByOrderId(it.id)
            return it.toOrderSummaryResponse(transactionalService.getTransactionByOrderId(it.id), products)
        }
        return null
    }
}
