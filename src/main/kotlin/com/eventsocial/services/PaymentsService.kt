package com.eventsocial.services

import com.eventsocial.clients.MercadoPagoApiClient
import com.eventsocial.clients.SengridApiClient
import com.eventsocial.clients.StripeApiClient
import com.eventsocial.domain.entities.Transactional
import com.eventsocial.domain.requests.CreateBookingPaymentStripe
import com.eventsocial.domain.requests.CreateOrderPaymentStripe
import com.eventsocial.domain.requests.CreatePaymentMP
import com.eventsocial.domain.requests.StatusChangePaymentMP
import com.eventsocial.domain.response.PaymentMP
import com.eventsocial.domain.response.PaymentStripe
import com.eventsocial.repositories.TransactionalRepository
import com.stripe.exception.SignatureVerificationException
import com.stripe.model.PaymentIntent
import com.stripe.net.Webhook
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import java.time.Instant
import java.util.*

@Service
class PaymentsService {

    @Autowired
    private lateinit var mercadoPagoApiClient: MercadoPagoApiClient

    @Autowired
    private lateinit var stripeApiClient: StripeApiClient

    @Autowired
    private lateinit var transactionalRepository: TransactionalRepository

    @Autowired
    private lateinit var bookingService: BookingService

    @Autowired
    private lateinit var orderService: OrderService

    @Autowired
    private lateinit var sengridApiClient: SengridApiClient

    fun paymentsMPService(
        createPaymentMP: CreatePaymentMP,
        userCode: UUID
    ): PaymentMP {
        bookingService.getBookingByCodeAndUserCode(createPaymentMP.bookingCode, userCode)?.let { booking ->

            transactionalRepository.findByBookingId_Id(booking.id)?.let { transactional ->
                mercadoPagoApiClient.paymentsMP(createPaymentMP, userCode, booking).let {
                    transactional.requestId = it.id
                    transactional.paymentStatus = BookingService.StatusOwnerApproval.CREATED.name
                    transactional.amount = booking.amount
                    transactional.collectorId = it.collectorId.toString()
                    transactional.idempotencyId = it.externalReference
                    transactional.bookingId = booking
                    transactional.updatedAt = Instant.now()

                    transactionalRepository.save(transactional)

                    return PaymentMP(
                        code = "200",
                        urlDirPago = it.initPoint,
                        paymentStatus = "CREATED",
                        message = "re-dirigir",
                    )
                }
            } ?: mercadoPagoApiClient.paymentsMP(createPaymentMP, userCode, booking).let {
                transactionalRepository.save(
                    Transactional(
                        requestId = it.id,
                        paymentStatus = BookingService.StatusOwnerApproval.CREATED.name,
                        paymentAt = Instant.now(),
                        amount = booking.amount,
                        collectorId = it.collectorId.toString(),
                        idempotencyId = it.externalReference,
                        bookingId = booking,
                        currency = "ARS",
                        updatedAt = null,
                        orderId = null,
                    )
                )

                return PaymentMP(
                    code = "200",
                    urlDirPago = it.initPoint,
                    paymentStatus = "CREATED",
                    message = "re-dirigir",
                )
            }
        } ?: throw ResponseStatusException(
            HttpStatus.CONFLICT,
            "Booking code:${createPaymentMP.bookingCode}, not found"
        )
    }

    fun statuChangePayment(statusChangeInfo: StatusChangePaymentMP) {
        if (statusChangeInfo.topic == "payment") {
            mercadoPagoApiClient.dataPaymentMP(statusChangeInfo.resource!!).let { paymentInfo ->
                transactionalRepository.findByCollectorId(paymentInfo.body?.collection?.external_reference!!)
                    .let { transaction ->

                        transaction.paymentStatus = paymentInfo.body?.collection!!.status
                        transaction.currency = paymentInfo.body?.collection!!.currency_id.toString()
                        transaction.montoNetoRecibido = paymentInfo.body?.collection!!.net_received_amount
                        transaction.paymentId = paymentInfo.body?.collection!!.paymentId.toString()
                        transaction.paymentMethod = paymentInfo.body?.collection!!.paymentMethodId
                        transaction.paymentType = paymentInfo.body?.collection!!.paymentType

                        transactionalRepository.save(transaction)

                        if (transaction.paymentStatus == "approved") {
                            // Se envia el email al organizer
                            sengridApiClient.bookingStatusOrganizer(transaction.bookingId!!, null)

                            // Se envia el email al owner
                            sengridApiClient.bookingStatusOwner(transaction.bookingId!!)
                        }
                    }
            }
        }
    }

    fun getPaymentMPByBookingCodeOrganizer(bookingCode: UUID, userCode: UUID) =
        transactionalRepository.findByBookingId_CodeAndBookingId_UserId_Code(bookingCode, userCode)

    fun getPaymentMPByBookingCodeOwner(bookingCode: UUID, userCode: UUID) =
        transactionalRepository.findByBookingId_CodeAndBookingId_OwnerUserId_Code(bookingCode, userCode)

    fun createPaymentStripeByBookingId(createBookingPaymentStripe: CreateBookingPaymentStripe, userCode: UUID): PaymentStripe {

        bookingService.getBookingByCodeAndUserCode(createBookingPaymentStripe.bookingCode, userCode)?.let { booking ->
            transactionalRepository.findByBookingId_Id(booking.id)?.let { transactional ->
                stripeApiClient.createPayment(createBookingPaymentStripe, userCode, booking).let {
                    transactional.requestId = it!!.id
                    transactional.paymentStatus = BookingService.StatusOwnerApproval.CREATED.name
                    transactional.amount = booking.amount
                    transactional.currency = it.currency
                    transactional.paymentStatus = it.status
                    transactional.bookingId = booking
                    transactional.updatedAt = Instant.now()
                    transactionalRepository.save(transactional)

                    return PaymentStripe(
                        code = "200",
                        urlDirPago = it.url,
                        paymentStatus = "CREATED",
                        message = "re-dirigir",
                    )
                }
            } ?: stripeApiClient.createPayment(createBookingPaymentStripe, userCode, booking).let {
                transactionalRepository.save(
                    Transactional(
                        requestId = it!!.id,
                        paymentStatus = it.paymentStatus,
                        paymentAt = Instant.now(),
                        amount = booking.amount,
                        collectorId = "",
                        idempotencyId = "",
                        bookingId = booking,
                        currency = it.currency,
                        updatedAt = null
                    )
                )

                return PaymentStripe(
                    code = "200",
                    urlDirPago = it.url,
                    paymentStatus = "CREATED",
                    message = "re-dirigir",
                )
            }
        } ?: throw ResponseStatusException(
            HttpStatus.CONFLICT,
            "Booking code:${createBookingPaymentStripe.bookingCode}, not found"
        )
    }

    fun createPaymentStripeByOrder(createOrderPaymentStripe: CreateOrderPaymentStripe, userCode: UUID): PaymentStripe {
        orderService.getOrderByCodeAndUserCode(createOrderPaymentStripe.orderCode, userCode)?.let { order ->
            transactionalRepository.findByOrderId_Id(order.id)?.let { transactional ->
                stripeApiClient.createOrderPayment(createOrderPaymentStripe, userCode, order).let {
                    transactional.requestId = it!!.id
                    transactional.paymentStatus = BookingService.StatusOwnerApproval.CREATED.name
                    transactional.amount = order.totalPrice.toFloat()
                    transactional.currency = it.currency
                    transactional.paymentStatus = it.status
                    transactional.orderId = order
                    transactional.updatedAt = Instant.now()
                    transactionalRepository.save(transactional)

                    return PaymentStripe(
                        code = "200",
                        urlDirPago = it.url,
                        paymentStatus = "CREATED",
                        message = "re-dirigir",
                    )
                }
            } ?: stripeApiClient.createOrderPayment(createOrderPaymentStripe, userCode, order).let {
                transactionalRepository.save(
                    Transactional(
                        requestId = it!!.id,
                        paymentStatus = it.paymentStatus,
                        paymentAt = Instant.now(),
                        amount = order.totalPrice.toFloat(),
                        collectorId = "",
                        idempotencyId = "",
                        orderId = order,
                        currency = it.currency,
                        updatedAt = null,
                        bookingId = null,
                    )
                )

                return PaymentStripe(
                    code = "200",
                    urlDirPago = it.url,
                    paymentStatus = "CREATED",
                    message = "re-dirigir",
                )
            }
        } ?: throw ResponseStatusException(
            HttpStatus.CONFLICT,
            "Order code:${createOrderPaymentStripe.orderCode}, not found"
        )
    }

    fun stripeWebhookEvent(eventJson: String, signature: String) {
        // Verificar la firma del evento utilizando la clave secreta de Stripe
        val endpointSecret = "whsec_dT3oQyXeDrOzZmZwYOaNNz77UA0Yd1rY"
        try {
            val event = Webhook.constructEvent(eventJson, signature, endpointSecret)

            if (event.type == "payment_intent.succeeded") {
                val paymentData = (event.data.`object` as PaymentIntent)

                val bookingCode = paymentData.metadata["booking_code"].toString()
                var transactional = transactionalRepository.findByBookingId_Code(UUID.fromString(bookingCode))

                if (transactional == null) {
                    val orderCode = paymentData.metadata["order_code"].toString()
                    transactional = transactionalRepository.findByOrderId_Code(UUID.fromString(orderCode))
                }

                // Evento de pago exitoso
                // Procesar el pago y actualizar la reserva correspondiente
                transactional?.apply {
                    paymentAt = Instant.now()
                    paymentStatus = paymentData.status
                    paymentMethod = paymentData.paymentMethod
                    approvedPayment = Instant.now()
                    montoNetoRecibido = paymentData.amount.toFloat()
                    paymentId = paymentData.id
                    paymentType = paymentData.paymentMethodTypes.toString()
                    updatedAt = Instant.now()
                    idempotencyId = event.request.idempotencyKey
                    transactionalRepository.save(this)
                }
            } else if (event.type == "payment_intent.payment_failed") {

                val paymentData = (event.data.`object` as PaymentIntent)
                val bookingCode = paymentData.metadata["booking_code"].toString()

                // Evento de pago fallido
                // Manejar el pago fallido y realizar acciones adicionales si es necesario
                // ...
                // Actualizar los datos en la entidad Transactional
                val transactional = transactionalRepository.findByBookingId_Code(UUID.fromString(bookingCode))

                transactional?.apply {
                    paymentAt = Instant.now()
                    paymentStatus = "FAILED: " + paymentData.status
                    idempotencyId = event.request.idempotencyKey
                    updatedAt = Instant.now()
                    transactionalRepository.save(this)
                }
            }
        } catch (e: SignatureVerificationException) {
            throw InvalidPropertiesFormatException("Firma del evento inválida.")
        }
    }
}
