package com.eventsocial.services

import com.eventsocial.domain.entities.ProductOrder
import com.eventsocial.repositories.ProductOrderRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service
import java.util.*
import javax.persistence.EntityNotFoundException

@Service
class ProductOrderService {

    @Autowired
    private lateinit var productOrderRepository: ProductOrderRepository

    fun getProductOrderById(id: Long): ProductOrder = productOrderRepository.findById(id)
        .orElseThrow { EntityNotFoundException("Product Order with id $id not found") }

    fun createProductOrder(productOrder: ProductOrder): ProductOrder {
        return productOrderRepository.save(productOrder)
    }

    fun getProductsByUserCode(userCode: UUID) = productOrderRepository.findByOrder_UserCode(userCode)

    fun getProductsByOrderId(orderId: Long) = productOrderRepository.findByOrderId(orderId)

    fun getProductsByUserCodeWithPage(userCode: UUID, offset: Int, page: Int) =
        productOrderRepository.findByOrder_UserCode(userCode, PageRequest.of(page, offset))
}
