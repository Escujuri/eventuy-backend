package com.eventsocial.services

import com.eventsocial.domain.entities.Product
import com.eventsocial.repositories.ProductRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.persistence.EntityNotFoundException

@Service
class ProductService {

    @Autowired
    private lateinit var productRepository: ProductRepository

    fun getProductById(id: Long): Product = productRepository.findById(id)
        .orElseThrow { EntityNotFoundException("Product with id $id not found") }
}
