package com.eventsocial.services

import com.eventsocial.clients.SengridApiClient
import com.eventsocial.config.ConfigCommon
import com.eventsocial.domain.entities.Register
import com.eventsocial.domain.entities.Users
import com.eventsocial.domain.response.OTPResponse
import com.eventsocial.domain.response.RegisterResponse
import com.eventsocial.exceptions.CustomException
import com.eventsocial.repositories.RegisterRepository
import com.eventsocial.repositories.UserRepository
import com.eventsocial.security.jwt.JwtRequestStep1
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import java.time.Instant
import java.util.*
import javax.transaction.Transactional

@Service("RegisterService")
class RegisterService {

    @Autowired
    lateinit var registerRepository: RegisterRepository

    @Autowired
    lateinit var userRepository: UserRepository

    @Autowired
    lateinit var sengridApiClient: SengridApiClient

    @Autowired
    lateinit var configCommon: ConfigCommon

    @Transactional(rollbackOn = [Exception::class])
    fun singUp(jwtRequestStep1: JwtRequestStep1): RegisterResponse {
        registerRepository.findByEmailAndValidated(jwtRequestStep1.email!!)?.let {
            throw ResponseStatusException(HttpStatus.CONFLICT, "email: ${jwtRequestStep1.email} is already registered")
        } ?: registerRepository.findByEmail(jwtRequestStep1.email!!)?.let {
            if (it.otpCode.equals(jwtRequestStep1.otpCode!!)) {
                registerRepository.save(
                    Register(
                        id = it.id,
                        code = it.code,
                        password = configCommon.hashWith256(jwtRequestStep1.password).toString(),
                        email = jwtRequestStep1.email!!,
                        validated = true
                    )
                ).let {
                    userRepository.save(
                        Users(
                            name = jwtRequestStep1.name,
                            nickName = jwtRequestStep1.username,
                            lastName = jwtRequestStep1.lastName,
                            phone = jwtRequestStep1.phone,
                            countryPhoneCode = jwtRequestStep1.countryPhoneCode,
                            birthDate = jwtRequestStep1.birthday,
                            registerId = it,
                            favorites = emptyArray(),
                            updatedAt = null
                        )
                    )
                }
                sengridApiClient.postSendEmail(jwtRequestStep1.email, jwtRequestStep1.username)

                return RegisterResponse(
                    message = "Se registro correctamente",
                    userCaseId = "usuario_guardado",
                    null
                )
            } else {
                throw ResponseStatusException(HttpStatus.CONFLICT, "Code is wrong for email: ${jwtRequestStep1.email}")
            }
        } ?: throw ResponseStatusException(HttpStatus.CONFLICT, "Email: ${jwtRequestStep1.email} not found")
    }

    @Transactional(rollbackOn = [Exception::class])
    fun getOTPEmail(email: String): OTPResponse {

        registerRepository.findByEmailAndValidated(email)?.let {
            val infoOTP = registerRepository.save(
                Register(
                    id = it.id,
                    code = it.code,
                    email = it.email,
                    password = it.password,
                    otpCode = String.format("%04d", Random().nextInt(10000)),
                    dateValidationOTP = Instant.now(),
                    validated = true,
                )
            )

            sengridApiClient.postOTPEmail(email, infoOTP.otpCode!!)

            return OTPResponse(
                code = "200",
                message = "Se envio el codigo otp al email :$email"
            )
        } ?: throw ResponseStatusException(HttpStatus.CONFLICT, "Email= $email not found")
    }

    @Transactional(rollbackOn = [Exception::class])
    fun getValidateOTPByEmailAndCode(email: String, otpCode: String): OTPResponse {
        registerRepository.findByEmailAndValidated(email)?.let {
            if (it.otpCode == otpCode) {

                val calendar_ = Calendar.getInstance()
                val calendar = Calendar.getInstance()

                calendar.time = Date() // Configuramos la fecha que se recibe
                calendar_.add(Calendar.MINUTE, 3) // Sumo 3 minutos a la fecha de creacion

                if (calendar_.after(calendar)) {

                    val newPass = UUID.randomUUID().toString().replace("-", "").substring(0, 8)

                    registerRepository.save(
                        Register(
                            id = it.id,
                            password = configCommon.hashWith256(newPass)!!,
                            email = it.email,
                            code = it.code,
                            validated = it.validated
                        )
                    )

                    sengridApiClient.postOTPEmail(email, newPass)

                    return OTPResponse(
                        code = "200",
                        message = "Se envio la nueva contraseña al email: $email"
                    )
                } else {
                    throw ResponseStatusException(HttpStatus.CONFLICT, "OTP EXPIRED")
                }
            } else {
                throw ResponseStatusException(HttpStatus.CONFLICT, "OTP IS WRONG")
            }
        } ?: throw CustomException("Email= $email not found")
    }

    @Transactional(rollbackOn = [Exception::class])
    fun getValidateEmail(email: String): OTPResponse {
        registerRepository.findByEmail(email)?.let {
            if (it.validated) {
                throw ResponseStatusException(HttpStatus.CONFLICT, "Email: $email already exist")
            } else {
                val infoOTP = registerRepository.save(
                    Register(
                        id = it.id,
                        code = it.code,
                        email = email,
                        otpCode = String.format("%04d", Random().nextInt(10000)),
                        validated = it.validated
                    )
                )
                sengridApiClient.postOTPEmail(email, infoOTP.otpCode!!)

                return OTPResponse(
                    code = "200",
                    message = "Se envio el codigo otp de validacion al email :$email"
                )
            }
        }

        val infoOTP = registerRepository.save(
            Register(
                email = email,
                otpCode = String.format("%04d", Random().nextInt(10000))
            )
        )
        sengridApiClient.postOTPEmail(email, infoOTP.otpCode!!)

        return OTPResponse(
            code = "200",
            message = "Se envio el codigo otp de validacion al email :$email"
        )
    }
}
