package com.eventsocial.services

import com.eventsocial.domain.mappers.ReviewMapper.toReviewCompanyReponse
import com.eventsocial.domain.mappers.ReviewMapper.toReviewSpaceReponse
import com.eventsocial.domain.requests.CreateReviewsCompanyRequest
import com.eventsocial.domain.requests.CreateReviewsRequest
import com.eventsocial.domain.response.ReviewCompanyReponse
import com.eventsocial.domain.response.ReviewSpaceReponse
import com.eventsocial.repositories.ReviewsCompaniesRepository
import com.eventsocial.repositories.ReviewsRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import java.util.*

@Service
class ReviewsService {

    @Autowired
    private lateinit var reviewsRepository: ReviewsRepository

    @Autowired
    private lateinit var reviewsCompaniesRepository: ReviewsCompaniesRepository

    @Autowired
    private lateinit var spaceService: SpaceService

    @Autowired
    private lateinit var companyService: CompanyService

    fun createReview(userCode: UUID, createReviewsRequest: CreateReviewsRequest): ReviewSpaceReponse {
        val space = spaceService.getSpaceByCode(createReviewsRequest.spaceCode) ?: throw ResponseStatusException(HttpStatus.CONFLICT, "Space code: ${createReviewsRequest.spaceCode} not found")

        val review = reviewsRepository.save(createReviewsRequest.toModel(userCode, space))
        updateRatingSpace(createReviewsRequest.spaceCode)
        return review.toReviewSpaceReponse()
    }

    fun createReviewCompany(userCode: UUID, createReviewsCompanyRequest: CreateReviewsCompanyRequest): ReviewCompanyReponse {
        val company = companyService.getCompanyByCode(createReviewsCompanyRequest.companyCode) ?: throw ResponseStatusException(HttpStatus.CONFLICT, "Company code: ${createReviewsCompanyRequest.companyCode} not found")

        val review = reviewsCompaniesRepository.save(createReviewsCompanyRequest.toModel(userCode, company))
        updateRatingCompany(createReviewsCompanyRequest.companyCode)
        return review.toReviewCompanyReponse()
    }

    fun updateRatingCompany(serviceCode: UUID) {
        var rating: Float = 0F
        var counter: Int = 0

        reviewsCompaniesRepository.findByCompanyId_ServiceCode(serviceCode)?.let { review ->
            review.map {
                counter++
                rating += it!!.rating
            }
        }
        if (counter >= 1)
            companyService.updateRatingCompany(serviceCode, rating / counter, counter)
    }

    fun updateRatingSpace(spaceCode: UUID) {
        var rating: Float = 0F
        var counter: Int = 0

        reviewsRepository.findBySpaceId_Code(spaceCode)?.let { review ->
            review.map {
                counter++
                rating += it!!.rating
            }
        }
        if (counter >= 1)
            spaceService.updateRatingSpace(spaceCode, rating / counter, counter)
    }

    fun getReviewsBySpaceCode(spaceCode: UUID) =
        reviewsRepository.findBySpaceId_Code(spaceCode)

    fun getReviewsByCompanyCode(companyCode: UUID) =
        reviewsCompaniesRepository.findByCompanyId_ServiceCode(companyCode)
}
