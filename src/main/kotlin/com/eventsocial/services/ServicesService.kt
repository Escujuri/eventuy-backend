package com.eventsocial.services

import com.eventsocial.domain.entities.Services
import com.eventsocial.repositories.ServiceRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ServicesService {

    @Autowired
    private lateinit var serviceRepository: ServiceRepository

    fun getAllServices(): List<Services> {
        return serviceRepository.findAll()
    }

    fun getServiceById(id: Long): Services? {
        return serviceRepository.findById(id).orElse(null)
    }

    fun createService(services: Services): Services {
        return serviceRepository.save(services)
    }

    fun updateService(id: Long, services: Services): Services? {
        val serviceToUpdate = getServiceById(id)
        if (serviceToUpdate != null) {
            serviceToUpdate.name = services.name
            serviceToUpdate.description = services.description
            return serviceRepository.save(serviceToUpdate)
        }
        return null
    }

    fun deleteService(id: Long) {
        serviceRepository.deleteById(id)
    }
}
