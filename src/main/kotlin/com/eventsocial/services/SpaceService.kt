package com.eventsocial.services

import com.eventsocial.domain.entities.Spaces
import com.eventsocial.domain.requests.PatchSpaceRequest
import com.eventsocial.domain.requests.PutSpaceRequest
import com.eventsocial.domain.requests.SpaceRequest
import com.eventsocial.exceptions.CustomException
import com.eventsocial.repositories.SpaceRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.Instant
import java.util.*

@Service
class SpaceService {

    @Autowired
    lateinit var spaceRepository: SpaceRepository
    @Transactional
    fun createSpace(userCode: UUID, spaceRequest: SpaceRequest): Spaces {
        return spaceRepository.save(spaceRequest.toModel(userCode))
    }
    @Transactional
    fun getSpacesWithPagination(userCode: UUID, offset: Int, page: Int) = spaceRepository.findByUserCode(
        userCode,
        PageRequest.of(page, offset, Sort.by("createdAt"))
    )

    @Transactional
    fun getSpaces(userCode: UUID) = spaceRepository.findByUserCode(userCode)

    @Transactional
    fun getSpaceByCode(spaceCode: UUID): Spaces? =
        spaceRepository.findByCode(spaceCode) ?: throw CustomException("Space code:$spaceCode, not found")

    @Transactional
    fun deleteSpace(userCode: UUID, spaceCode: UUID) {
        spaceRepository.findByCode(spaceCode)?.let {
            if (it.userCode == userCode) {
                spaceRepository.deleteByCode(spaceCode)
            } else {
                throw CustomException("the space code:$spaceCode does not belong to the user code:$userCode")
            }
        } ?: throw CustomException("Space code:$spaceCode, not found")
    }
    @Transactional
    fun putSpace(spaceCode: UUID, putSpaceRequest: PutSpaceRequest): Spaces {
        spaceRepository.findByCode(spaceCode)?.let {
            return spaceRepository.save(
                Spaces(
                    title = putSpaceRequest.title,
                    spaceType = putSpaceRequest.typeSpace.name,
                    street = putSpaceRequest.adress.street,
                    cp = putSpaceRequest.adress.cp,
                    number = putSpaceRequest.adress.number,
                    location = putSpaceRequest.adress.location,
                    city = putSpaceRequest.adress.city,
                    province = putSpaceRequest.adress.province,
                    country = putSpaceRequest.adress.country,
                    description = putSpaceRequest.description,
                    price = putSpaceRequest.price,
                    maximumOccupancy = putSpaceRequest.maximumOccupancy,
                    hoursAvailability = putSpaceRequest.hoursAvailability,
                    services = putSpaceRequest.services,
                    securityService = putSpaceRequest.securityService,
                    images = putSpaceRequest.images,
                    priceType = putSpaceRequest.priceType,
                    cleanPrice = putSpaceRequest.cleanPrice,
                    publish = putSpaceRequest.publish,
                    datesNotAvailable = putSpaceRequest.datesNotAvailable,
                    nameContact = putSpaceRequest.nameContact,
                    lastNameContact = putSpaceRequest.lastNameContact,
                    telephoneContact = putSpaceRequest.telephoneContact,
                    openHours = putSpaceRequest.openHours,
                    cancellationsType = putSpaceRequest.cancellationsType,
                    rankingAmount = it.rankingAmount,
                    code = it.code,
                    userCode = it.userCode,
                    enableSpace = true,
                    updatedAt = Instant.now(),
                    minimumReservationHours = putSpaceRequest.minimumReservationHours,
                    overNightAllowed = putSpaceRequest.overNightAllowed
                )
            )
        } ?: throw CustomException("Space code:$spaceCode, not found")
    }


    @Transactional
    fun patchSpace(spaceCode: UUID, patchSpaceRequest: PatchSpaceRequest): Spaces {
        return spaceRepository.findByCode(spaceCode)?.let { existingSpace ->
            existingSpace.apply {
                id = id
                code = code
                title = patchSpaceRequest.title ?: title
                spaceType = patchSpaceRequest.typeSpace?.name ?: spaceType
                street = patchSpaceRequest.adress?.street ?: street
                cp = patchSpaceRequest.adress?.cp ?: cp
                number = patchSpaceRequest.adress?.number ?: number
                location = patchSpaceRequest.adress?.location ?: location
                city = patchSpaceRequest.adress?.city ?: city
                province = patchSpaceRequest.adress?.province ?: province
                country = patchSpaceRequest.adress?.country ?: country
                description = patchSpaceRequest.description ?: description
                price = patchSpaceRequest.price ?: price
                maximumOccupancy = patchSpaceRequest.maximumOccupancy ?: maximumOccupancy
                hoursAvailability = patchSpaceRequest.hoursAvailability ?: hoursAvailability
                services = patchSpaceRequest.services ?: services
                securityService = patchSpaceRequest.securityService ?: securityService
                images = patchSpaceRequest.images ?: images
                priceType = patchSpaceRequest.priceType ?: priceType
                cleanPrice = patchSpaceRequest.cleanPrice ?: cleanPrice
                publish = patchSpaceRequest.publish ?: publish
                datesNotAvailable = patchSpaceRequest.datesNotAvailable ?: datesNotAvailable
                nameContact = patchSpaceRequest.nameContact ?: nameContact
                lastNameContact = patchSpaceRequest.lastNameContact ?: lastNameContact
                telephoneContact = patchSpaceRequest.telephoneContact ?: telephoneContact
                openHours = patchSpaceRequest.openHours ?: openHours
                cancellationsType = patchSpaceRequest.cancellationsType ?: cancellationsType
                updatedAt = Instant.now()
                minimumReservationHours = patchSpaceRequest.minimumReservationHours ?: minimumReservationHours
                overNightAllowed = patchSpaceRequest.overNightAllowed ?: overNightAllowed
            }
            spaceRepository.save(existingSpace)
        } ?: throw CustomException("Space code:$spaceCode, not found")
    }

    @Transactional
    fun updateRatingSpace(spaceCode: UUID, ranking: Float, counter: Int): Spaces? {
        spaceRepository.findByCode(spaceCode)?.let {
            it.ranking = ranking
            it.rankingAmount = counter
            return spaceRepository.save(it)
        } ?: throw CustomException("Space code:$spaceCode, not found")
    }
}
