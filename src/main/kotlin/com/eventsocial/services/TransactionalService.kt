package com.eventsocial.services

import com.eventsocial.repositories.TransactionalRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class TransactionalService {

    @Autowired
    private lateinit var transactionalRepository: TransactionalRepository

    enum class StatusPayment(val paymentStatus: String?) {
        UNPAID("OPEN"),
        REJECTED("UNPAID"),
        SUCCESSFUL("PAID"),
        SUCCEEDED("SUCCEEDED");
    }

    @Transactional(rollbackFor = [Exception::class])
    fun getTransactionByBookingId(bookingId: Long) =
        StatusPayment.values().first { it.paymentStatus == transactionalRepository.findByBookingId_Id(bookingId)?.paymentStatus?.uppercase() ?: "OPEN" }

    @Transactional(rollbackFor = [Exception::class])
    fun getTransactionByOrderId(orderId: Long) =
        StatusPayment.values().first { it.paymentStatus == transactionalRepository.findByOrderId_Id(orderId)?.paymentStatus?.uppercase() ?: "OPEN" }
}
