package com.eventsocial.services

import com.eventsocial.config.ConfigCommon
import com.eventsocial.domain.entities.Users
import com.eventsocial.domain.mappers.UsersMapper.toUserSummeryReponse
import com.eventsocial.domain.requests.PatchUserRequest
import com.eventsocial.domain.response.UsersChatResponse
import com.eventsocial.repositories.BookingRepository
import com.eventsocial.repositories.RegisterRepository
import com.eventsocial.repositories.SpaceRepository
import com.eventsocial.repositories.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.Instant
import java.util.*

@Service
class UserService {

    @Autowired
    lateinit var userRepository: UserRepository

    @Autowired
    lateinit var registerRepository: RegisterRepository

    @Autowired
    lateinit var spaceRepository: SpaceRepository

    @Autowired
    private lateinit var bookingRepository: BookingRepository

    @Autowired
    lateinit var configCommon: ConfigCommon

    @Transactional
    fun getUser(userCode: UUID) = userRepository.findByCode(userCode)

    @Transactional
    fun getAllUser() = userRepository.findAll()

    @Transactional
    fun pathUser(userCode: UUID, patchUserRequest: PatchUserRequest): Users {
        userRepository.findByCode(userCode).let {
            return userRepository.save(
                Users(
                    id = it.id,
                    code = it.code,
                    name = patchUserRequest.name ?: it.name,
                    lastName = patchUserRequest.lastName ?: it.lastName,
                    icon = patchUserRequest.icon ?: it.icon,
                    registerId = it.registerId,
                    age = patchUserRequest.age ?: it.age,
                    phone = patchUserRequest.phone ?: it.phone,
                    birthDate = patchUserRequest.birthDate ?: it.birthDate,
                    documento = patchUserRequest.documento ?: it.documento,
                    favorites = if (patchUserRequest.favorites.isNullOrEmpty()) it.favorites else patchUserRequest.favorites.toTypedArray(),
                    updatedAt = Instant.now()
                )
            )
        }
    }
    @Transactional
    fun getListInfoUserOrganizer(userId: String): Any =
        bookingRepository.findBookingPaidByUserId(userId.toInt()).map {
            return UsersChatResponse(
                userContacts = listOf(it.ownerUserId.toUserSummeryReponse())
            )
        }

    @Transactional
    fun getListInfoUserOwner(userId: String): Any =
        bookingRepository.findBookingPaidByUserIdOwner(userId.toInt()).map {
            return UsersChatResponse(
                userContacts = listOf(it.userId.toUserSummeryReponse())
            )
        }
}
