create table if not exists users(
                      id bigserial primary key,
                      email varchar unique,
                      name varchar,
                      lastName varchar,
                      last_name varchar,
                      age int,
                      phone varchar,
                      code UUID unique,
                      icon varchar,
                      created_at TIMESTAMP,
                      updated_at TIMESTAMP,
                      favorites text[]
)

ALTER TABLE users ADD constraint pk_id_email UNIQUE (id,email)

create table if not exists spaces(
                      id bigserial primary key,
                      email varchar unique,
                      dates_not_available text[],
                      services text[],
                      security_services text[],
                      images text[],
                      open_hours Jsonb
)


CREATE TABLE public.booking (
	id bigserial NOT NULL,
	created_at timestamp NOT NULL,
	spaces_id int8 NOT NULL,
	users_id int8 NOT NULL,
	code uuid NOT NULL,
    date_and_time Jsonb,
	maximum_occupancy int4 NULL,
	owner_approval varchar(255) NOT NULL,
	owner_user_id int8 NOT NULL,
	CONSTRAINT booking_pkey PRIMARY KEY (id)
);


-- public.booking foreign keys

ALTER TABLE public.booking ADD CONSTRAINT fk158twsxs1y4jvfd99k821m4e4 FOREIGN KEY (users_id) REFERENCES public.users(id);
ALTER TABLE public.booking ADD CONSTRAINT fkca824fp0tymi1ny8vlnc5made FOREIGN KEY (spaces_id) REFERENCES public.spaces(id);
ALTER TABLE public.booking ADD CONSTRAINT fkelyfkl8guadnr592axcf62lv8 FOREIGN KEY (owner_user_id) REFERENCES public.users(id);



CREATE TABLE public.transactional (
	id bigserial NOT NULL,
	amount numeric(19, 2) NULL,
	approved_payment timestamp NULL,
	code uuid NOT NULL,
	collector_id varchar(255) NULL,
	currency_id varchar(255) NULL,
	idempotency_id varchar(255) NOT NULL,
	monto_neto_recibido float4 NULL,
	payment_at timestamp NULL,
	payment_id varchar(255) NULL,
	payment_method varchar(255) NULL,
	payment_status varchar(255) NULL,
	payment_type varchar(255) NULL,
	payment_type_id varchar(255) NULL,
	request_id varchar(255) NULL,
	booking_id int8 NOT NULL,
	CONSTRAINT transactional_pkey PRIMARY KEY (id),
	CONSTRAINT uk_ga3vsney413b9j1ihucy2ju3d UNIQUE (booking_id)
);


-- public.transactional foreign keys
ALTER TABLE public.transactional ADD CONSTRAINT fkoi70gg19ky52am45xfqnxdc40 FOREIGN KEY (booking_id) REFERENCES public.booking(id);


-- SERVICES
create table if not exists Service (
    id bigserial PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    description VARCHAR(255)
);

create table if not exists Product (
    id bigserial PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    description VARCHAR(255),
    price DOUBLE PRECISION NOT NULL,
    currency VARCHAR(10) NOT NULL
);

create table if not exists Service_Order (
    id bigserial PRIMARY KEY,
    service_id INTEGER NOT NULL,
    quantity INTEGER NOT NULL,
    total_price DOUBLE PRECISION NOT NULL,
    order_id INTEGER NOT NULL,
    FOREIGN KEY (service_id) REFERENCES Service (id) ON DELETE CASCADE,
    FOREIGN KEY (order_id) REFERENCES "Order" (id) ON DELETE CASCADE
);

create table if not exists "Order" (
   id bigserial PRIMARY KEY,
   total_price DOUBLE PRECISION NOT NULL,
    currency VARCHAR(10) NOT NULL,
    code uuid NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP,
    street VARCHAR NOT NULL,
    number int NOT NULL,
    location VARCHAR NOT NULL,
    city VARCHAR NOT NULL,
    province VARCHAR NOT NULL,
    country VARCHAR NOT NULL,
    cp VARCHAR NOT NULL
    )

create table if not exists Product_Order (
    id bigserial PRIMARY KEY,
    product_id INTEGER NOT NULL,
    quantity INTEGER NOT NULL,
    total_price DOUBLE PRECISION NOT NULL,
    order_id INTEGER NOT NULL,
    FOREIGN KEY (product_id) REFERENCES products (id) ON DELETE CASCADE,
    FOREIGN KEY (order_id) REFERENCES "Order" (id) ON DELETE CASCADE
    )

-- public.transactional definition

-- Drop table

-- DROP TABLE public.transactional;


CREATE table order_transactional (
                                     id bigserial NOT NULL,
                                     amount numeric(19, 2) NULL,
                                     approved_payment timestamp NULL,
                                     code uuid NOT NULL,
                                     collector_id varchar(255) NULL,
                                     currency_id varchar(255) NULL,
                                     idempotency_id varchar(255) NOT NULL,
                                     amount_net_received float4 NULL,
                                     payment_at timestamp NULL,
                                     payment_id varchar(255) NULL,
                                     payment_method varchar(255) NULL,
                                     payment_status varchar(255) NULL,
                                     payment_type varchar(255) NULL,
                                     payment_type_id varchar(255) NULL,
                                     request_id varchar(255) NULL,
                                     user_order_id int8 NOT NULL,
                                     created_at timestamp NOT NULL,
                                     updated_at timestamp NULL,
                                     currency varchar(255) NULL,
                                     CONSTRAINT order_transactional_pkey PRIMARY KEY (id)
)
ALTER TABLE public.order_transactional ADD CONSTRAINT fk_user_order_id FOREIGN KEY (user_order_id) REFERENCES public.user_order(id);
